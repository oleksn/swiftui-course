//
//  _33_Locking_our_UI_behind_Face_ID_Bucket_List_12_12App.swift
//  133 Locking our UI behind Face ID Bucket List 12 12
//
//  Created by Oleksandr Nesynov on 01.03.2023.
//

import SwiftUI

@main
struct _33_Locking_our_UI_behind_Face_ID_Bucket_List_12_12App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
