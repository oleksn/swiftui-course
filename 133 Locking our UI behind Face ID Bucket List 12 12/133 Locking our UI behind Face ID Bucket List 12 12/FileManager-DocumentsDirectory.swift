//
//  FileManager-DocumentsDirectory.swift
//  133 Locking our UI behind Face ID Bucket List 12 12
//
//  Created by Oleksandr Nesynov on 01.03.2023.
//

import Foundation

extension FileManager {
    static var documentsDirectory: URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
}

