//
//  _04_Creating_NSManagedObject_subclasses_Core_Data_2_7App.swift
//  104 Creating NSManagedObject subclasses Core Data 2 7
//
//  Created by Oleks on 16.02.2023.
//

import SwiftUI

@main
struct _04_Creating_NSManagedObject_subclasses_Core_Data_2_7App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
