//
//  _06_Ensuring_Core_Data_objects_are_unique_using_constraints_Core_Data_4_7App.swift
//  106 Ensuring Core Data objects are unique using constraints Core Data 4 7
//
//  Created by Oleks on 16.02.2023.
//

import SwiftUI

@main
struct _06_Ensuring_Core_Data_objects_are_unique_using_constraints_Core_Data_4_7App: App {
    let persistenceContainer = DataController()
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(
                    \.managedObjectContext,
                     persistenceContainer
                        .container
                        .viewContext
                )
        }
    }
}
