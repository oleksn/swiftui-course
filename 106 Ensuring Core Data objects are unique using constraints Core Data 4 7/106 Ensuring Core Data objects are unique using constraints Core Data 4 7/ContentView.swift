//
//  ContentView.swift
//  106 Ensuring Core Data objects are unique using constraints Core Data 4 7
//
//  Created by Oleks on 16.02.2023.
//

import SwiftUI

struct ContentView: View {
    @Environment(\.managedObjectContext) var moc
    @FetchRequest(sortDescriptors: []) var wizards: FetchedResults<Wizard>
    
    var body: some View {
        VStack {
            List(wizards, id: \.self) { wizard in
                Text(wizard.name ?? "Unknown")
            }
        }
        
        Button("Add") {
            let wizard = Wizard(context: moc)
            wizard.name = "Harry Potter"
        }
        
        Button("Save") {
            do {
                try moc.save()
            } catch {
                print(error.localizedDescription)
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
