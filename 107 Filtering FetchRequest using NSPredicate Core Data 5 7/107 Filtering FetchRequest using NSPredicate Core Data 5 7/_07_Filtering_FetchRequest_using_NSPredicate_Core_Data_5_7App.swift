//
//  _07_Filtering_FetchRequest_using_NSPredicate_Core_Data_5_7App.swift
//  107 Filtering FetchRequest using NSPredicate Core Data 5 7
//
//  Created by Oleks on 16.02.2023.
//

import SwiftUI

@main
struct _07_Filtering_FetchRequest_using_NSPredicate_Core_Data_5_7App: App {
    let persistenceContainer = DataController()
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(
                    \.managedObjectContext,
                     persistenceContainer
                        .container
                        .viewContext
                )
        }
    }
}
