//
//  ContentView.swift
//  107 Filtering FetchRequest using NSPredicate Core Data 5 7
//
//  Created by Oleks on 16.02.2023.
//

import SwiftUI

struct ContentView: View {
    @Environment(\.managedObjectContext) var moc
    @FetchRequest(
        sortDescriptors: [],
        predicate: NSPredicate(
            format: "universe == 'Star Trek 2'"
        )
//        predicate: nil
    ) var ships: FetchedResults<Ship>
    
    var body: some View {
        VStack {
            List(ships, id:\.self) { ship in
                Text(ship.name ?? "Unknown name")
            }
            
            Button("Add Examples") {
                let ship1 = Ship(context: moc)
                ship1.name = "Enterprise"
                ship1.universe = "Star Trek"
                
                let ship2 = Ship(context: moc)
                ship2.name = "Enterprise 2"
                ship2.universe = "Star Trek 2"
                
                let ship3 = Ship(context: moc)
                ship3.name = "Enterprise 3"
                ship3.universe = "Star Trek 3"
                
                let ship4 = Ship(context: moc)
                ship4.name = "Enterprise 4"
                ship4.universe = "Star Trek 4"
                
                try? moc.save()
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
