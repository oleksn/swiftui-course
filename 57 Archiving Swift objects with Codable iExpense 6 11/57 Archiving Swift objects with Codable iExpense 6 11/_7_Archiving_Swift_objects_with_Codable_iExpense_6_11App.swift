//
//  _7_Archiving_Swift_objects_with_Codable_iExpense_6_11App.swift
//  57 Archiving Swift objects with Codable iExpense 6 11
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _7_Archiving_Swift_objects_with_Codable_iExpense_6_11App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
