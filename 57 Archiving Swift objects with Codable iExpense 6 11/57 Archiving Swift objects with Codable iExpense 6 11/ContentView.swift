//
//  ContentView.swift
//  57 Archiving Swift objects with Codable iExpense 6 11
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

struct User: Codable {
    let firstName: String
    let lastName: String
}

struct ContentView: View {
    @State private var user = User(
        firstName: "taylor", lastName: "Swift"
    )
    
    var body: some View {
        Button("Save User") {
            let encode = JSONEncoder()
            if let data = try? encode.encode(user) {
                UserDefaults.standard.set(data, forKey: "UserData")
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
