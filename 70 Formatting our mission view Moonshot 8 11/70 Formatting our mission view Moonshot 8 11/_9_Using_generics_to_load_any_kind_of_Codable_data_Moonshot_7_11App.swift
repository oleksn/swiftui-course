//
//  _9_Using_generics_to_load_any_kind_of_Codable_data_Moonshot_7_11App.swift
//  69 Using generics to load any kind of Codable data Moonshot 7 11
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _9_Using_generics_to_load_any_kind_of_Codable_data_Moonshot_7_11App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
