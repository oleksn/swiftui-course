//
//  Astronaut.swift
//  68 Loading a specific kind of Codable data Moonshot 6 11
//
//  Created by Oleks on 03.02.2023
//

import Foundation

struct Astronaut: Codable, Identifiable {
    let id: String
    let name: String
    let description: String
}
