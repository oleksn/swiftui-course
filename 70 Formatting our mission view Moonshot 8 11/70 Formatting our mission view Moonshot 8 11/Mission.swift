//
//  Mission.swift
//  69 Using generics to load any kind of Codable data Moonshot 7 11
//
//  Created by Oleks on 03.02.2023
//

import Foundation

struct Mission: Codable, Identifiable {
    struct CrewRole: Codable {
        let name: String
        let role: String
    }

    let id: Int
    let launchDate: String?
    let crew: [CrewRole]
    let description: String
}
