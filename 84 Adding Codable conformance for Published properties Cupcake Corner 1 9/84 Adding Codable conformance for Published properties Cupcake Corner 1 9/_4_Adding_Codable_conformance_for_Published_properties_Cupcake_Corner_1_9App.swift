//
//  _4_Adding_Codable_conformance_for_Published_properties_Cupcake_Corner_1_9App.swift
//  84 Adding Codable conformance for Published properties Cupcake Corner 1 9
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _4_Adding_Codable_conformance_for_Published_properties_Cupcake_Corner_1_9App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
