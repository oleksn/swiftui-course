//
//  _51_Dynamically_filtering_a_List_Hot_Prospects_12_18App.swift
//  151 Dynamically filtering a List Hot Prospects 12 18
//
//  Created by Oleksandr Nesynov on 03.03.2023.
//

import SwiftUI

@main
struct _51_Dynamically_filtering_a_List_Hot_Prospects_12_18App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
