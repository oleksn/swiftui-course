//
//  ContentView.swift
//  7 Reading text from the user with TextField WeSplit 7 11
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

struct ContentView: View {
    @State var check = 0.0
    @State var number = 2
    @State var tip = 20
    
    let tips = [10, 15, 20, 25, 0]
    
    var body: some View {
        NavigationView {
            Form {
                Section {
                    if #available(iOS 16, *) {
                        TextField("Amount", value: $check,
                                  //                    format: .currency(code: "USD")
                                  format: .currency(
                                    code: Locale.current.currency?.identifier ?? "USD"
                                  )
                        )
                        .keyboardType(.decimalPad)
                    } else {
                        // Fallback on earlier versions
                    }
                    
                    Picker("Number", selection: $number) {
                        ForEach(2..<100) {
                            Text("\($0) people")
                        }
                    }
                }
                
                Section {
                    Text(check, format: .currency(
                        code: Locale.current.currency?.identifier ?? "USD"
                    ))
                }
            }
        }.navigationTitle("navigationTitle")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
