//
//  _2_Merging_Codable_structs_Moonshot_10_11App.swift
//  72 Merging Codable structs Moonshot 10 11
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _2_Merging_Codable_structs_Moonshot_10_11App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
