//
//  _4_Showing_and_hiding_views_iExpense_3_11App.swift
//  54 Showing and hiding views iExpense 3 11
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _4_Showing_and_hiding_views_iExpense_3_11App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
