//
//  _7_Views_as_properties_Views_and_Modifiers_7_10App.swift
//  27 Views as properties Views and Modifiers 7 10
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _7_Views_as_properties_Views_and_Modifiers_7_10App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
