//
//  ContentView.swift
//  27 Views as properties Views and Modifiers 7 10
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

struct ContentView: View {
////    1)
//    let motto1 = Text("Draco dormiens")
//    let motto2 = Text("nunquam titillandus")
    
    var motto1: some View {
        Text("Draco dormiens")
    }
    let motto2 = Text("nunquam titillandus")

////      3)
//    var spells: some View {
//////      2)
////        VStack {
////            Text("Lumos")
////            Text("Obliviate")
////        }
//        Group {
//            Text("Lumos")
//            Text("Obliviate")
//        }
//    }
    
    @ViewBuilder var spells: some View {
        Text("Lumos")
        Text("Obliviate")
    }
    
    var body: some View {
////      2)
//        HStack {
        VStack {
////            1)
//            motto1
//                .foregroundColor(.red)
//            motto2
//                .foregroundColor(.blue)
            spells
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
