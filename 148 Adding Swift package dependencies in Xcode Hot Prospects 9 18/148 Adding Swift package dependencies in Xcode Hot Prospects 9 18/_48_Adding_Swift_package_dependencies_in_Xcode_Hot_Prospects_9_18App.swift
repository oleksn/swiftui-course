//
//  _48_Adding_Swift_package_dependencies_in_Xcode_Hot_Prospects_9_18App.swift
//  148 Adding Swift package dependencies in Xcode Hot Prospects 9 18
//
//  Created by Oleksandr Nesynov on 03.03.2023.
//

import SwiftUI

@main
struct _48_Adding_Swift_package_dependencies_in_Xcode_Hot_Prospects_9_18App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
