//
//  _3_Creating_a_spirograph_with_Drawing_10_10App.swift
//  83 Creating a spirograph with Drawing 10 10
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _3_Creating_a_spirograph_with_Drawing_10_10App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
