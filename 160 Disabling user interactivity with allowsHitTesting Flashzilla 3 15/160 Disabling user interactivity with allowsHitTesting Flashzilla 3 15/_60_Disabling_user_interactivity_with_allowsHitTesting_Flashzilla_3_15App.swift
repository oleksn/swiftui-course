//
//  _60_Disabling_user_interactivity_with_allowsHitTesting_Flashzilla_3_15App.swift
//  160 Disabling user interactivity with allowsHitTesting Flashzilla 3 15
//
//  Created by Oleksandr Nesynov on 03.03.2023.
//

import SwiftUI

@main
struct _60_Disabling_user_interactivity_with_allowsHitTesting_Flashzilla_3_15App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
