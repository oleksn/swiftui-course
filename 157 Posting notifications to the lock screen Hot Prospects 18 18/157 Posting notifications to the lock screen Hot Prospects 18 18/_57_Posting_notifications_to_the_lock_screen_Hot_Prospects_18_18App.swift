//
//  _57_Posting_notifications_to_the_lock_screen_Hot_Prospects_18_18App.swift
//  157 Posting notifications to the lock screen Hot Prospects 18 18
//
//  Created by Oleksandr Nesynov on 03.03.2023.
//

import SwiftUI

@main
struct _57_Posting_notifications_to_the_lock_screen_Hot_Prospects_18_18App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
