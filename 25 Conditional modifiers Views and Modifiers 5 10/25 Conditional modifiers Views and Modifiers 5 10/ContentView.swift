//
//  ContentView.swift
//  25 Conditional modifiers Views and Modifiers 5 10
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

struct ContentView: View {
    @State var useRedText = false
    
    var body: some View {
        Button("Hello") {
            useRedText.toggle()
        }
        .foregroundColor(useRedText ? .red : .blue)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
