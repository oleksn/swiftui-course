//
//  _5_Conditional_modifiers_Views_and_Modifiers_5_10App.swift
//  25 Conditional modifiers Views and Modifiers 5 10
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _5_Conditional_modifiers_Views_and_Modifiers_5_10App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
