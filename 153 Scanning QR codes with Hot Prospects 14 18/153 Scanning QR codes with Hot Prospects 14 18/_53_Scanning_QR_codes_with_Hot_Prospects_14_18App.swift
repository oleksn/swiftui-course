//
//  _53_Scanning_QR_codes_with_Hot_Prospects_14_18App.swift
//  153 Scanning QR codes with Hot Prospects 14 18
//
//  Created by Oleksandr Nesynov on 03.03.2023.
//

import SwiftUI

@main
struct _53_Scanning_QR_codes_with_Hot_Prospects_14_18App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
