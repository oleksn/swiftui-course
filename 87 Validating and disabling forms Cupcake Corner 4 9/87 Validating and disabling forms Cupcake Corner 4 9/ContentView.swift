//
//  ContentView.swift
//  87 Validating and disabling forms Cupcake Corner 4 9
//
//  Created by Oleks on 07.02.2023.
//

import SwiftUI

struct ContentView: View {
    @State var userName = ""
    @State var email = ""
    
    var body: some View {
        Form {
            Section {
                TextField("UserName", text: $userName)
                TextField("Email", text: $email)
            }
            
            Section {
                Button("Create account") {
                    print("Creating account....")
                }
            }
            .disabled(disableForm)
        }
    }
    
    var disableForm: Bool {
        userName.count < 5 || email.count < 5
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
