//
//  _7_Validating_and_disabling_forms_Cupcake_Corner_4_9App.swift
//  87 Validating and disabling forms Cupcake Corner 4 9
//
//  Created by Oleks on 07.02.2023.
//

import SwiftUI

@main
struct _7_Validating_and_disabling_forms_Cupcake_Corner_4_9App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
