//
//  _67_Coloring_views_as_we_swipe_Flashzilla_10_15App.swift
//  167 Coloring views as we swipe Flashzilla 10 15
//
//  Created by Oleksandr Nesynov on 03.03.2023.
//

import SwiftUI

@main
struct _67_Coloring_views_as_we_swipe_Flashzilla_10_15App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
