//
//  _8_Controlling_the_animation_stack_Animation_5_8App.swift
//  48 Controlling the animation stack Animation 5 8
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _8_Controlling_the_animation_stack_Animation_5_8App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
