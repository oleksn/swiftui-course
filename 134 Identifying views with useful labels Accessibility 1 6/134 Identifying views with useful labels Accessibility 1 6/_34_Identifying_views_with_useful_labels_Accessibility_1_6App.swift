//
//  _34_Identifying_views_with_useful_labels_Accessibility_1_6App.swift
//  134 Identifying views with useful labels Accessibility 1 6
//
//  Created by Oleksandr Nesynov on 02.03.2023.
//

import SwiftUI

@main
struct _34_Identifying_views_with_useful_labels_Accessibility_1_6App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
