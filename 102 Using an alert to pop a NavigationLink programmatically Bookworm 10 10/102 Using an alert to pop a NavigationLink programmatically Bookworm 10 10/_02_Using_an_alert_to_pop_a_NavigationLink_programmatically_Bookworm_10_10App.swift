//
//  _02_Using_an_alert_to_pop_a_NavigationLink_programmatically_Bookworm_10_10App.swift
//  102 Using an alert to pop a NavigationLink programmatically Bookworm 10 10
//
//  Created by Oleks on 16.02.2023.
//

import SwiftUI

@main
struct _02_Using_an_alert_to_pop_a_NavigationLink_programmatically_Bookworm_10_10App: App {
    let persistenceContainer = DataController()
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(
                    \.managedObjectContext,
                     persistenceContainer
                        .container
                        .viewContext
                )
        }
    }
}
