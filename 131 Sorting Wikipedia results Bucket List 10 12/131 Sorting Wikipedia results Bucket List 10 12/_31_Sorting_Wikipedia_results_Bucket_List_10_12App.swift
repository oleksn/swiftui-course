//
//  _31_Sorting_Wikipedia_results_Bucket_List_10_12App.swift
//  131 Sorting Wikipedia results Bucket List 10 12
//
//  Created by Oleksandr Nesynov on 01.03.2023.
//

import SwiftUI

@main
struct _31_Sorting_Wikipedia_results_Bucket_List_10_12App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
