//
//  ContentView.swift
//  143 Understanding Swifts Result type Hot Prospects 4 18
//
//  Created by Oleksandr Nesynov on 03.03.2023.
//

import SwiftUI

struct ContentView: View {
    @State var output = ""
    
    var body: some View {
        Text(output)
            .task {
                await fetchReadings()
            }
    }
    
    func fetchReadings() async {
//      // 1)
//        do {
//            let url = URL(string: "https://hws.dev/readings.json")!
//            let (data, _) = try await URLSession.shared.data(from: url)
//            let readings = try JSONDecoder().decode([Double].self, from: data)
//            output = "Found \(readings.count) readings."
//        } catch {
//            print("Download Error")
//        }
//      // 2)
        let fetchTask = Task { () -> String in
            let url = URL(string: "https://hws.dev/readings.json")!
            let (data, _) = try await URLSession.shared.data(from: url)
            let readings = try JSONDecoder().decode([Double].self, from: data)
            return "Found \(readings.count) readings."
        }
        
//        fetchTask.cancel()
        
        let result = await fetchTask.result
        
//      // 3)
//        do {
//            output = try result.get()
//        } catch {
//            print("Download Error")
//        }
        
        switch result {
        case .success(let str):
            output = str
        case .failure(let error):
            print("Download Error: \(error.localizedDescription)")
        }
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
