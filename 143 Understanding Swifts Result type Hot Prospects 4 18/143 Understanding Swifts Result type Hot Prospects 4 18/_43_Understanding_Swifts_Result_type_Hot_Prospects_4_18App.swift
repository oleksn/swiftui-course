//
//  _43_Understanding_Swifts_Result_type_Hot_Prospects_4_18App.swift
//  143 Understanding Swifts Result type Hot Prospects 4 18
//
//  Created by Oleksandr Nesynov on 03.03.2023.
//

import SwiftUI

@main
struct _43_Understanding_Swifts_Result_type_Hot_Prospects_4_18App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
