//
//  _3_Colors_and_frames_Guess_the_Flag_2_9App.swift
//  13 Colors and frames Guess the Flag 2 9
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _3_Colors_and_frames_Guess_the_Flag_2_9App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
