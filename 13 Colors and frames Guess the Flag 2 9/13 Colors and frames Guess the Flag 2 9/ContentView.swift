//
//  ContentView.swift
//  13 Colors and frames Guess the Flag 2 9
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

struct ContentView: View {
    var body: some View {
//        1)
//        ZStack {
////            Color.primary
////            Color.secondary
//            Color.init(red: 1, green: 0.8, blue: 1)
//                .ignoresSafeArea()
//            Color.red
////            .frame(width: 200,height: 200)
//                .frame(minWidth: 100,
//                       maxWidth: .infinity,
//                       maxHeight: 200)
//            Text("Content")
////            .background(.red)
//        }
////        .background(.red)
//        2)
        ZStack {
//            VStack {
            VStack(spacing: 0) {
                Color.red
                Color.blue
            }
            
            Text("Content")
//                .foregroundColor(.yellow)
                .foregroundStyle(.secondary)
                .padding(50)
                .background(.ultraThinMaterial)
        }.ignoresSafeArea()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
