//
//  _79_Working_with_two_side_by_side_views_in_SnowSeeker_1_11App.swift
//  179 Working with two side by side views in SnowSeeker 1 11
//
//  Created by Oleksandr Nesynov on 10.03.2023.
//

import SwiftUI

@main
struct _79_Working_with_two_side_by_side_views_in_SnowSeeker_1_11App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
