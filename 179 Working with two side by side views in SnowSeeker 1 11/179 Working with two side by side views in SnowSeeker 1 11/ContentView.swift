//
//  ContentView.swift
//  179 Working with two side by side views in SnowSeeker 1 11
//
//  Created by Oleksandr Nesynov on 10.03.2023.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
////    1)
//        NavigationView {
//            Text("Hello, wolrld")
//                .navigationTitle("Primary")
//
//            Text("Secondary")
//        }
        
////    2)
        NavigationView {
            NavigationLink {
                Text("New secondary")
            } label: {
                Text("Hello, world")
            }
            .navigationTitle("Primary")
            
            Text("Secondary")
            
            Text("Tertiary")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
