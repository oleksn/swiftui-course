//
//  _45_Creating_context_menus_Hot_Prospects_6_18App.swift
//  145 Creating context menus Hot Prospects 6 18
//
//  Created by Oleksandr Nesynov on 03.03.2023.
//

import SwiftUI

@main
struct _45_Creating_context_menus_Hot_Prospects_6_18App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
