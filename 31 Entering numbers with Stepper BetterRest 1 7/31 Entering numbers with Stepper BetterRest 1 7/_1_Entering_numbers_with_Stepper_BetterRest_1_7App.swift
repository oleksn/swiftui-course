//
//  _1_Entering_numbers_with_Stepper_BetterRest_1_7App.swift
//  31 Entering numbers with Stepper BetterRest 1 7
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _1_Entering_numbers_with_Stepper_BetterRest_1_7App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
