//
//  ContentView.swift
//  31 Entering numbers with Stepper BetterRest 1 7
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

struct ContentView: View {
    @State var sleepAmount = 8.0
    
    var body: some View {
        Stepper(
            "\(sleepAmount) hours",
            value: $sleepAmount,
            in: 4...12,
            step: 0.25
        )
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
