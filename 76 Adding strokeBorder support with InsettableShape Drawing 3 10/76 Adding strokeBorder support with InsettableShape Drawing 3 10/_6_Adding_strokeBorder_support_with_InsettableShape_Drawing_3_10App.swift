//
//  _6_Adding_strokeBorder_support_with_InsettableShape_Drawing_3_10App.swift
//  76 Adding strokeBorder support with InsettableShape Drawing 3 10
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _6_Adding_strokeBorder_support_with_InsettableShape_Drawing_3_10App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
