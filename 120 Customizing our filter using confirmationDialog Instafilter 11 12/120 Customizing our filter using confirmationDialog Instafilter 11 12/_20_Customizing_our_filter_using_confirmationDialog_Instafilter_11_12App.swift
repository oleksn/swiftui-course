//
//  _20_Customizing_our_filter_using_confirmationDialog_Instafilter_11_12App.swift
//  120 Customizing our filter using confirmationDialog Instafilter 11 12
//
//  Created by Oleks on 17.02.2023.
//

import SwiftUI

@main
struct _20_Customizing_our_filter_using_confirmationDialog_Instafilter_11_12App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
