//
//  ContentView.swift
//  29 Custom modifiers Views and Modifiers 9 10
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

//// Ex1
struct Title: ViewModifier {
    func body(content: Content) -> some View {
        content
            .font(.largeTitle)
            .foregroundColor(.white)
            .padding()
            .background(.blue)
            .clipShape(RoundedRectangle(cornerRadius: 10))
    }
}

//// Ex1
extension View {
    func titleStyle() -> some View {
        modifier(Title())
    }
}

//// Ex2
struct Watermark: ViewModifier {
    var text: String
    
    func body(content: Content) -> some View {
        ZStack(alignment: .bottomTrailing) {
            content
            
            Text(text)
                .font(.caption)
                .foregroundColor(.white)
                .padding(5)
                .background(.black)
        }
    }
}

//// Ex2
extension View {
    func watermarked(with text: String) -> some View {
        modifier(Watermark(text: text))
    }
}


struct ContentView: View {
    var body: some View {
////    1)
//        Text("Hello, world!")
//            .modifier(Title())
//        2)
//        Text("Hello, world!")
//            .titleStyle()
        Color.blue
            .frame().frame(width: 300, height: 200)
            .watermarked(with: "Hacking with Swift")
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
