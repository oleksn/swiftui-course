//
//  _9_Custom_modifiers_Views_and_Modifiers_9_10App.swift
//  29 Custom modifiers Views and Modifiers 9 10
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _9_Custom_modifiers_Views_and_Modifiers_9_10App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
