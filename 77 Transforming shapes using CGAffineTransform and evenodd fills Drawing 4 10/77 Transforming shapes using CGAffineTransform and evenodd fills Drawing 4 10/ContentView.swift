//
//  ContentView.swift
//  77 Transforming shapes using CGAffineTransform and evenodd fills Drawing 4 10
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

struct Flower: Shape {
    var petalOffset = -20.0
    var pedalWidth = 100.0
    
    func path(in rect: CGRect) -> Path {
        var path = Path()
        
        for number in stride(
            from: 0,
            to: Double.pi * 2,
            by: Double.pi / 8
        ) {
            let rotation = CGAffineTransform(rotationAngle: number)
            let position = rotation.concatenating(
                CGAffineTransform(
                    translationX: rect.width / 2,
                    y: rect.height / 2
                )
            )
            
            let originalPetal = Path(ellipseIn: CGRect.init(
                x: petalOffset, y: 0.0,
                width: pedalWidth, height: rect.width / 2
            ))
            
            let rotatedPetal = originalPetal.applying(position)
            path.addPath(rotatedPetal)
        }
        
        return path
    }
}

struct ContentView: View {
    @State var petalOffset = -20.0
    @State var petalWidth = 100.0
    
    var body: some View {
        VStack {
            Flower(petalOffset: petalOffset, pedalWidth: petalWidth)
                .fill(
                    .red, style: FillStyle(
                        eoFill: true
                ))
//                .stroke(.red, lineWidth: 1)
                
            
            Text("Offset")
            
            Slider(value: $petalOffset, in: -40...40)
                .padding(.horizontal)
            
            Text("Width")
            Slider(value: $petalWidth, in: 0...100)
                .padding(.horizontal)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
