//
//  _7_Transforming_shapes_using_CGAffineTransform_and_evenodd_fills_Drawing_4_10App.swift
//  77 Transforming shapes using CGAffineTransform and evenodd fills Drawing 4 10
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _7_Transforming_shapes_using_CGAffineTransform_and_evenodd_fills_Drawing_4_10App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
