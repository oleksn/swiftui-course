//
//  ContentView.swift
//  64 How ScrollView lets us work with scrolling data Moonshot 2 11
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

struct CustomText: View {
    let text: String
    
    var body: some View {
        Text(text)
    }
    
    init(_ text: String) {
        print("Create new text")
        self.text = text
    }
}

struct ContentView: View {
    var body: some View {
//        ScrollView {
        ScrollView(.horizontal) {
//            VStack(spacing: 10) {
//            LazyVStack(spacing: 10) {
            LazyHStack(spacing: 10) {
                ForEach(0..<100) {
                    Text("Item \($0)")
                        .font(.title)
                }
            }
            .frame(maxWidth: .infinity)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
