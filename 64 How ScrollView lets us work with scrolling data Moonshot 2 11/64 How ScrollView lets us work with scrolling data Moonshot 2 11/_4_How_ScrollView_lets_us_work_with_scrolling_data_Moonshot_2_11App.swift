//
//  _4_How_ScrollView_lets_us_work_with_scrolling_data_Moonshot_2_11App.swift
//  64 How ScrollView lets us work with scrolling data Moonshot 2 11
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _4_How_ScrollView_lets_us_work_with_scrolling_data_Moonshot_2_11App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
