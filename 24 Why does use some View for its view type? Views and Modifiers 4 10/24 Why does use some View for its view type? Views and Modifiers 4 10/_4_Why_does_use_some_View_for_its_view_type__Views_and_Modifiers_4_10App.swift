//
//  _4_Why_does_use_some_View_for_its_view_type__Views_and_Modifiers_4_10App.swift
//  24 Why does use some View for its view type? Views and Modifiers 4 10
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _4_Why_does_use_some_View_for_its_view_type__Views_and_Modifiers_4_10App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
