//
//  _09_Onetomany_relationships_with_FetchRequest_and_Core_Data_7_7App.swift
//  109 Onetomany relationships with FetchRequest and Core Data 7 7
//
//  Created by Oleks on 17.02.2023.
//

import SwiftUI

@main
struct _09_Onetomany_relationships_with_FetchRequest_and_Core_Data_7_7App: App {
    let persistenceContainer = DataController()
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(
                    \.managedObjectContext,
                     persistenceContainer
                        .container
                        .viewContext
                )
        }
    }
}
