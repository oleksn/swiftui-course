//
//  WelcomeView.swift
//  184 Making NavigationView work in landscape SnowSeeker 6 11
//
//  Created by Oleksandr Nesynov on 10.03.2023.
//

import SwiftUI

struct WelcomeView: View {
    var body: some View {
        VStack {
            Text("Hello, World!")
                .font(.largeTitle)
            
            Text("Please select a resort the left-hard menu; swipe from the left edge to show it.")
                .foregroundColor(.secondary)
            
        }
    }
}

struct WelcomeView_Previews: PreviewProvider {
    static var previews: some View {
        WelcomeView()
    }
}
