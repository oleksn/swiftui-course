//
//  _86_Searching_for_data_in_a_List_SnowSeeker_8_11App.swift
//  186 Searching for data in a List SnowSeeker 8 11
//
//  Created by Oleksandr Nesynov on 10.03.2023.
//

import SwiftUI

@main
struct _86_Searching_for_data_in_a_List_SnowSeeker_8_11App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
