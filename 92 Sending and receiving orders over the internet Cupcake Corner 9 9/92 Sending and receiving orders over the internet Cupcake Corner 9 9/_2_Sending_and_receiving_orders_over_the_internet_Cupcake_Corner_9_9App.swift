//
//  _2_Sending_and_receiving_orders_over_the_internet_Cupcake_Corner_9_9App.swift
//  92 Sending and receiving orders over the internet Cupcake Corner 9 9
//
//  Created by Oleks on 16.02.2023.
//

import SwiftUI

@main
struct _2_Sending_and_receiving_orders_over_the_internet_Cupcake_Corner_9_9App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
