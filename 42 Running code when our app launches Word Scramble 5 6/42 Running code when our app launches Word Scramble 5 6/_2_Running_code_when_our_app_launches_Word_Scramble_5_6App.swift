//
//  _2_Running_code_when_our_app_launches_Word_Scramble_5_6App.swift
//  42 Running code when our app launches Word Scramble 5 6
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _2_Running_code_when_our_app_launches_Word_Scramble_5_6App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
