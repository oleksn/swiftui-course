//
//  _1_Why_does_use_structs_for_views__Views_and_Modifiers_1_10App.swift
//  21 Why does use structs for views? Views and Modifiers 1 10
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _1_Why_does_use_structs_for_views__Views_and_Modifiers_1_10App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
