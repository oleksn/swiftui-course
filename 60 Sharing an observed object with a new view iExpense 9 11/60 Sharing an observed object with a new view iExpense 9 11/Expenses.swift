//
//  Expenses.swift
//  58 Building a list we can delete from iExpense 7 11
//
//  Created by Oleks on 03.02.2023
//

import Foundation

class Expenses: ObservableObject {
     @Published var items = [ExpenseItem]()
}
