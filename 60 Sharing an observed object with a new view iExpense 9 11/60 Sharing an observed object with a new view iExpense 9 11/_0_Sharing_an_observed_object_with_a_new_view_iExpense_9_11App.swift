//
//  _0_Sharing_an_observed_object_with_a_new_view_iExpense_9_11App.swift
//  60 Sharing an observed object with a new view iExpense 9 11
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _0_Sharing_an_observed_object_with_a_new_view_iExpense_9_11App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
