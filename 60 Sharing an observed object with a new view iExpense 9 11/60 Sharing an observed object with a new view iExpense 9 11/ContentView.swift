//
//  ContentView.swift
//  60 Sharing an observed object with a new view iExpense 9 11
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

struct ContentView: View {
    @StateObject var expenses = Expenses()
    
    var body: some View {
        NavigationView {
            List {
                ForEach(expenses.items) { item in
                    Text(item.name)
                }
                .onDelete(perform: removeItems)
            }
        }
        .navigationTitle("iExpenses")
        .toolbar {
            Button {
                let expense = ExpenseItem(
                    name: "Test",
                    type: "Personal",
                    amount: 5
                )
                expenses.items.append(expense)
            } label: {
                Image(systemName: "plus")
            }
        }
    }
    
    func removeItems(at offsets: IndexSet) {
        expenses.items.remove(atOffsets: offsets)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
