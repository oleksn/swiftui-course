//
//  ContentView.swift
//  147 Scheduling local notifications Hot Prospects 8 18
//
//  Created by Oleksandr Nesynov on 03.03.2023.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            Button("Request Permission") {
                // first
                
                UNUserNotificationCenter
                    .current()
                    .requestAuthorization(
                        options: [.alert, .badge]
                    ) { success, error in
                        if success {
                            print("All set!")
                        } else if let error = error {
                            print(error.localizedDescription)
                        }
                    }
            }
            
            Button("Schedule Notification") {
                // second
                
                let content = UNMutableNotificationContent()
                content.title = "Feed the dogs"
                content.subtitle = "They look hungry"
                content.sound = UNNotificationSound.default
                
                let trigger = UNTimeIntervalNotificationTrigger(
                    timeInterval: 5,
                    repeats: false
                )
                
                let request = UNNotificationRequest(
                    identifier: UUID().uuidString,
                    content: content,
                    trigger: trigger
                )
                
                UNUserNotificationCenter.current().add(request)
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
