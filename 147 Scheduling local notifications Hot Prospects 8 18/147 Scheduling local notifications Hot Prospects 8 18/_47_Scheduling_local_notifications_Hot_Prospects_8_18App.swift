//
//  _47_Scheduling_local_notifications_Hot_Prospects_8_18App.swift
//  147 Scheduling local notifications Hot Prospects 8 18
//
//  Created by Oleksandr Nesynov on 03.03.2023.
//

import SwiftUI

@main
struct _47_Scheduling_local_notifications_Hot_Prospects_8_18App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
