//
//  ContentView.swift
//  5 Creating pickers in a form WeSplit 8 11
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

struct ContentView: View {
    let student = ["1", "2", "3"]
    @State var selectedStudent = "1"
    
    var body: some View {
        Form {
            Picker("Select", selection: $selectedStudent) {
                ForEach(student, id: \.self) {
                    Text($0)
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
