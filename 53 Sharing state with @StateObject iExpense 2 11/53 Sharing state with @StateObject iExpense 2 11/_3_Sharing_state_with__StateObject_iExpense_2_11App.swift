//
//  _3_Sharing_state_with__StateObject_iExpense_2_11App.swift
//  53 Sharing state with @StateObject iExpense 2 11
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _3_Sharing_state_with__StateObject_iExpense_2_11App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
