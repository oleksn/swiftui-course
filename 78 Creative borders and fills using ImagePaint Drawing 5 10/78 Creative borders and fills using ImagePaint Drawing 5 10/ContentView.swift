//
//  ContentView.swift
//  78 Creative borders and fills using ImagePaint Drawing 5 10
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

struct ContentView: View {
    var body: some View {
////    1)
//        Text("Hello, world!")
//            .frame().frame(width: 300, height: 300)
////            .background(.red)
////            .border(.red, width: 30)
////            .background(Image("Example")) // error
////            .border(ImagePaint(
////                image: Image("Example"),
////                scale: 0.2),
////                width: 50
////            )
//            .border(ImagePaint(image: Image("Example"),sourceRect: CGRect(
//                x: 0, y: 0.25, width: 1, height: 0.25
//            ), scale: 0.2),
//                    width: 50
//            )
////    2)
        Capsule()
            .border(ImagePaint(image: Image("Example"),sourceRect: CGRect(
                x: 0, y: 0.25, width: 1, height: 0.5
            ), scale: 0.2),
                    width: 50
            )
            .frame(width: 300, height: 300)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
