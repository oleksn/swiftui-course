//
//  _8_Creative_borders_and_fills_using_ImagePaint_Drawing_5_10App.swift
//  78 Creative borders and fills using ImagePaint Drawing 5 10
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _8_Creative_borders_and_fills_using_ImagePaint_Drawing_5_10App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
