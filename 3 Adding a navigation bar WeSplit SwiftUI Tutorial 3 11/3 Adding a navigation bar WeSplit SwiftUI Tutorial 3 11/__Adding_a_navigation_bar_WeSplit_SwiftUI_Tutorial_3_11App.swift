//
//  __Adding_a_navigation_bar_WeSplit_SwiftUI_Tutorial_3_11App.swift
//  3 Adding a navigation bar WeSplit SwiftUI Tutorial 3 11
//
//  Created by Oleks on 27.01.2023.
//

import SwiftUI

@main
struct __Adding_a_navigation_bar_WeSplit_SwiftUI_Tutorial_3_11App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
