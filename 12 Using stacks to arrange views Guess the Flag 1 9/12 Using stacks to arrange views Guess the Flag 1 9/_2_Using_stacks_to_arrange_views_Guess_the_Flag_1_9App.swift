//
//  _2_Using_stacks_to_arrange_views_Guess_the_Flag_1_9App.swift
//  12 Using stacks to arrange views Guess the Flag 1 9
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _2_Using_stacks_to_arrange_views_Guess_the_Flag_1_9App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
