//
//  ContentView.swift
//  12 Using stacks to arrange views Guess the Flag 1 9
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

struct ContentView: View {
    var body: some View {
/// 1)
//        HStack(spacing: 5) {
//            Text("Hello, world!1")
//            Text("Hello, world!2 Hello")
//        }
/// 2)
//        VStack(spacing: 5) {
//            Text("Hello, world!1")
//            Text("Hello, world!2 Hello")
//            Text("Hello, world!3 Hello Hello")
//            Spacer()
//        }
/// 3)
//        VStack(spacing: 5) {
//            Spacer()
//            Text("Hello, world!1")
//            Text("Hello, world!2 Hello")
//            Text("Hello, world!3 Hello Hello")
//            Spacer()
//            Spacer()
//        }
/// 4)
//        ZStack {
//            Text("Hello, world!1")
//            Text("Hello, world!2 Hello")
//        }
        
        VStack {
            HStack {
                Text("1")
                Text("2")
                Text("3")
            }
            
            HStack {
                Text("4")
                Text("5")
                Text("6")
            }
            
            HStack {
                Text("7")
                Text("8")
                Text("9")
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
