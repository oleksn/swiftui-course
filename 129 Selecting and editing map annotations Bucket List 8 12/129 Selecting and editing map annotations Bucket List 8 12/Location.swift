//
//  Location.swift
//  129 Selecting and editing map annotations Bucket List 8 12
//
//  Created by Oleksandr Nesynov on 23.02.2023.
//

import Foundation
import CoreLocation

struct Location: Identifiable, Codable, Equatable {
    let id: UUID
    let name: String
    var description: String
    let latitude: Double
    let longitude: Double
    
    var coordinate: CLLocationCoordinate2D {
        CLLocationCoordinate2D(
            latitude: latitude, longitude: longitude
        )
    }
    
    static let example = Location(
        id: UUID(),
        name: "Buckingham Palace",
        description: "Queen",
        latitude: 51.501,
        longitude: -0.141
    )
    
    static func ==(lhs: Location, rhs: Location) -> Bool {
        lhs.id == rhs.id
    }
}

