//
//  _29_Selecting_and_editing_map_annotations_Bucket_List_8_12App.swift
//  129 Selecting and editing map annotations Bucket List 8 12
//
//  Created by Oleksandr Nesynov on 23.02.2023.
//

import SwiftUI

@main
struct _29_Selecting_and_editing_map_annotations_Bucket_List_8_12App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
