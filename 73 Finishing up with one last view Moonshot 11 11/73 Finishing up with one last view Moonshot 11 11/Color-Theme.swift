//
//  Color-Theme.swift
//  73 Finishing up with one last view Moonshot 11 11
//
//  Created by Oleks on 03.02.2023
//

import Foundation
import SwiftUI

extension ShapeStyle where Self == Color {
    static var darkBackground: Color {
        Color(red: 0.1, green: 0.1, blue: 0.2)
    }

    static var lightBackground: Color {
        Color(red: 0.2, green: 0.2, blue: 0.3)
    }
}
