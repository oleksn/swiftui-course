//
//  _3_Finishing_up_with_one_last_view_Moonshot_11_11App.swift
//  73 Finishing up with one last view Moonshot 11 11
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _3_Finishing_up_with_one_last_view_Moonshot_11_11App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
