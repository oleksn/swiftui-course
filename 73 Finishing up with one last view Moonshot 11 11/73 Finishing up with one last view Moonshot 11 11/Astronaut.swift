//
//  Astronaut.swift
//  73 Finishing up with one last view Moonshot 11 11
//
//  Created by Oleks on 03.02.2023
//

import Foundation

struct Astronaut: Codable, Identifiable {
    let id: String
    let name: String
    let description: String
}
