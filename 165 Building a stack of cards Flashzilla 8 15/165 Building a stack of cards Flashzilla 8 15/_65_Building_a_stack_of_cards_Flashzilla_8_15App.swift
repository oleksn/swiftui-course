//
//  _65_Building_a_stack_of_cards_Flashzilla_8_15App.swift
//  165 Building a stack of cards Flashzilla 8 15
//
//  Created by Oleksandr Nesynov on 03.03.2023.
//

import SwiftUI

@main
struct _65_Building_a_stack_of_cards_Flashzilla_8_15App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
