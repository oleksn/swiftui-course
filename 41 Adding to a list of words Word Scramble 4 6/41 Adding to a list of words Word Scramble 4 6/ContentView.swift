//
//  ContentView.swift
//  41 Adding to a list of words Word Scramble 4 6
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

struct ContentView: View {
    @State var usedWords = [String]()
    @State var rootWord = ""
    @State var newWorld = ""
    
    var body: some View {
        NavigationView {
            List {
                Section {
                    TextField(
                        "Enter your world",
                        text: $newWorld
                    )
                    .autocapitalization(.none)
                }
                
                Section {
                    ForEach(usedWords, id: \.self) { word in
                        HStack {
                            Image(systemName: "\(word.count).circle")
                            Text(word)
                        }
                    }
                }
            }
            .navigationTitle(rootWord)
            .onSubmit(addNewWord)
        }
    }
    
    func addNewWord() {
        let answer = newWorld.lowercased().trimmingCharacters(
            in: .whitespacesAndNewlines
        )
        guard answer.count > 0 else {
            return
        }
        
        // validation
        
        withAnimation {
            usedWords.insert(answer, at: 0)
        }
        
        newWorld = ""
         
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
