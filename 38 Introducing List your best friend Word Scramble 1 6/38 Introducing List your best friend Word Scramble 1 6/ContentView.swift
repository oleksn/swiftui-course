//
//  ContentView.swift
//  38 Introducing List your best friend Word Scramble 1 6
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

struct ContentView: View {
    let people = ["Finn" , "Leia" , "Luke", "Rey"]
    var body: some View {
////      1)
//        List {
//            Section("Section 1") {
//                Text("Static row 1")
//                Text("Static row 2")
//                Text("Static row 3")
//            }
//
//            Section("Section 2") {
//                ForEach(0..<5) {
//                    Text("Dynamic row \($0)")
//                }
//            }
//
//            Section("Section 3") {
//                Text("Static row 4")
//                Text("Static row 5")
//                Text("Static row 6")
//            }
//        }
//        .listStyle(.grouped)
////      2)
//        List(0..<5) {
//            Text("Dynamic row \($0)")
//        }
//        .listStyle(.grouped)
////      3)
//        List(people, id: \.self) {
//            Text($0)
//        }
////      4)
        List {
            Text("Static row")
            
            ForEach(people, id: \.self) {
                Text("Dynamic row \($0)")
            }
            
            Text("Static row")
        }
        .listStyle(.grouped)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
