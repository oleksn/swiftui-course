//
//  _80_Using_alert_and_sheet_with_optionals_SnowSeeker_2_11App.swift
//  180 Using alert and sheet with optionals SnowSeeker 2 11
//
//  Created by Oleksandr Nesynov on 10.03.2023.
//

import SwiftUI

@main
struct _80_Using_alert_and_sheet_with_optionals_SnowSeeker_2_11App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
