//
//  ContentView.swift
//  180 Using alert and sheet with optionals SnowSeeker 2 11
//
//  Created by Oleksandr Nesynov on 10.03.2023.
//

import SwiftUI

struct User: Identifiable {
        var id = "Swift"
}

struct ContentView: View {
    @State var selectedUser: User? = User()
    @State var isShowingUser: Bool = false
    
    var body: some View {
        Text("Hello, world!")
            .onTapGesture {
                selectedUser = User()
                isShowingUser = true
            }
////        1)
//            .sheet(item: $selectedUser) { user in
//                Text(user.id)
//            }
////        2)
//            .sheet(isPresented: $isShowingUser) {
//                Text(selectedUser!.id)
//            }
////        3)
//            .alert(isPresented: $isShowingUser, present) {
//                Text(selectedUser!.id)
//            }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
