//
//  _23_Writing_data_to_the_documents_directory_Bucket_List_2_12App.swift
//  123 Writing data to the documents directory Bucket List 2 12
//
//  Created by Oleksandr Nesynov on 22.02.2023.
//

import SwiftUI

@main
struct _23_Writing_data_to_the_documents_directory_Bucket_List_2_12App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
