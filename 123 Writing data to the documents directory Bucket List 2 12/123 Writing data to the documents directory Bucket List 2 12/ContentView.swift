//
//  ContentView.swift
//  123 Writing data to the documents directory Bucket List 2 12
//
//  Created by Oleksandr Nesynov on 22.02.2023.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
            .onTapGesture {
                let str = "Text message"
                let url = getDocumentDirectory().appendingPathComponent("message.txt")
                do {
                    try str.write(
                        to: url,
                        atomically: true,
                        encoding: .utf8
                    )
                    
                    let input = try String(contentsOf: url)
                    print(input)
                } catch {
                    print(error.localizedDescription)
                }
            }
    }
    
    func getDocumentDirectory() -> URL {
        let paths = FileManager.default.urls(
            for: .documentDirectory, in: .userDomainMask
        )
        
        return paths[0]
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
