//
//  _85_Creating_a_secondary_view_for_NavigationView_SnowSeeker_7_11App.swift
//  185 Creating a secondary view for NavigationView SnowSeeker 7 11
//
//  Created by Oleksandr Nesynov on 10.03.2023.
//

import SwiftUI

@main
struct _85_Creating_a_secondary_view_for_NavigationView_SnowSeeker_7_11App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
