//
//  ContentView.swift
//  112 Showing multiple options with confirmationDialog Instafilter 3 12
//
//  Created by Oleks on 17.02.2023.
//

import SwiftUI

struct ContentView: View {
    @State var showingConfirmation = false
    @State var backgroundColor = Color.white
    
    var body: some View {
        VStack {
            Text("Hello, world!")
                .frame(width: 300, height: 300)
                .background(backgroundColor)
                .onTapGesture {
                    showingConfirmation = true
                }
                .confirmationDialog("Change backgroundColor", isPresented: $showingConfirmation) {
                    Button("Red") {
                        backgroundColor = .red
                    }
                    
                    Button("Green") {
                        backgroundColor = .green
                    }
                    
                    Button("Blue") {
                        backgroundColor = .blue
                    }
                    
                    Button("Cancel", role: .cancel) { }
                } message: {
                    Text("Select a new color")
                }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
