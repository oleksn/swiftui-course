//
//  _12_Showing_multiple_options_with_confirmationDialog_Instafilter_3_12App.swift
//  112 Showing multiple options with confirmationDialog Instafilter 3 12
//
//  Created by Oleks on 17.02.2023.
//

import SwiftUI

@main
struct _12_Showing_multiple_options_with_confirmationDialog_Instafilter_3_12App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
