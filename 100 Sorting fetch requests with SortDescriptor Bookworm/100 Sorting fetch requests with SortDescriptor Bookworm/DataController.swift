//
//  DataController.swift
//  95 How to combine Core Data and SwiftUI Bookworm 3 10
//
//  Created by Oleks on 16.02.2023.
//

import CoreData
import Foundation

class DataController: ObservableObject {
    let container = NSPersistentContainer(name: "Bookworm")
    
    init() {
        container.loadPersistentStores { description, error in
            if let error = error {
                print("Core Data Failed to load \(error.localizedDescription)")
            }
        }
    }
}
