//
//  _00_Sorting_fetch_requests_with_SortDescriptor_BookwormApp.swift
//  100 Sorting fetch requests with SortDescriptor Bookworm
//
//  Created by Oleks on 16.02.2023.
//

import SwiftUI

@main
struct _00_Sorting_fetch_requests_with_SortDescriptor_BookwormApp: App {
    let persistenceContainer = DataController()
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(
                    \.managedObjectContext,
                     persistenceContainer
                        .container
                        .viewContext
                )
        }
    }
}
