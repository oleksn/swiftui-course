//
//  _0_Working_with_strings_Word_Scramble_3_6App.swift
//  40 Working with strings Word Scramble 3 6
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _0_Working_with_strings_Word_Scramble_3_6App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
