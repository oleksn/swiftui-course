//
//  ContentView.swift
//  40 Working with strings Word Scramble 3 6
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            Image(systemName: "globe")
                .imageScale(.large)
                .foregroundColor(.accentColor)
            Text("Hello, world!")
        }
        .padding()
    }
    
    func test() {
////      1)
//        let input = "a b c"
//        let lettets = input.components(separatedBy: " ")
////      2)
//        let input = """
//a
//b
//c
//"""
//        let letters = input.components(separatedBy: "\n")
//        let letter = letters.randomElement()
//        let trimmed = letter?.trimmingCharacters(in: .whitespacesAndNewlines)
////      3)
//        let word = "swift"
//        let checker = UITextChecker()
//        let range = NSRange(location: 0, length: word.utf16.count)
//        let misselledRange = checker.rangeOfMisspelledWord(
//            in: word, range: range, startingAt: 0, wrap: false, language: "en"
//        )
//
//        let allGood = misselledRange.location == NSNotFound
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
