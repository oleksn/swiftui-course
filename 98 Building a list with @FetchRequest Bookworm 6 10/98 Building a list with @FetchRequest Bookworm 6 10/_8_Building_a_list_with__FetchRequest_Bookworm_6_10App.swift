//
//  _8_Building_a_list_with__FetchRequest_Bookworm_6_10App.swift
//  98 Building a list with @FetchRequest Bookworm 6 10
//
//  Created by Oleks on 16.02.2023.
//

import SwiftUI

@main
struct _8_Building_a_list_with__FetchRequest_Bookworm_6_10App: App {
    let persistenceContainer = DataController()
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceContainer.container.viewContext)
        }
    }
}
