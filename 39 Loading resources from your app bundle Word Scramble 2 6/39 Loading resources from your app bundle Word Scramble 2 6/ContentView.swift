//
//  ContentView.swift
//  39 Loading resources from your app bundle Word Scramble 2 6
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
    }
    
    func loadFile() {
        if let fileURL = Bundle.main.url(
            forResource: "some-file", withExtension: "txt"
        ) {
            if let fileContents = try? String(contentsOf: fileURL) {
                // load file
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
