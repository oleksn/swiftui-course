//
//  _9_Loading_resources_from_your_app_bundle_Word_Scramble_2_6App.swift
//  39 Loading resources from your app bundle Word Scramble 2 6
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _9_Loading_resources_from_your_app_bundle_Word_Scramble_2_6App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
