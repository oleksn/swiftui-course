//
//  _8_Showing_the_players_score_with_an_alert_Guess_the_Flag_7_9App.swift
//  18 Showing the players score with an alert Guess the Flag 7 9
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _8_Showing_the_players_score_with_an_alert_Guess_the_Flag_7_9App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
