//
//  ContentView.swift
//  86 Loading an image from a remote server Cupcake Corner 3 9
//
//  Created by Oleks on 07.02.2023.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
////        1))
//       AsyncImage(
//        url: URL(string: "https://hws.dev/img/logo.png"),
//        scale: 3
//       ) { image in
////           image
////               .resizableImage()
////               .scaledToFit()
//
//       } placeholder: {
////           Color.red
//           ProgressView()
//
//       }
//       .frame(width: 300, height: 200)
////        2)
//        AsyncImage(url: URL(string: "https://hws.dev/img/bad.png")) { phase in
//            if let image = phase.image {
//                image
//                    .resizable()
//                    .scaledToFit()
//            } else if phase.error != nil {
//                Text("There was an error loading the image.")
//            } else {
//                ProgressView()
//            }
//        }
//        .frame(width: 200, height: 200)
////       3)
        AsyncImage(url: URL(string: "https://hws.dev/img/bad.png")) { phase in
            if let image = phase.image {
                image
                    .resizable()
                    .scaledToFit()
            } else if phase.error != nil {
                Text("There was an error loading the image.")
            } else {
                ProgressView()
            }
        }
        .frame(width: 200, height: 200)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
