//
//  _6_Loading_an_image_from_a_remote_server_Cupcake_Corner_3_9App.swift
//  86 Loading an image from a remote server Cupcake Corner 3 9
//
//  Created by Oleks on 07.02.2023.
//

import SwiftUI

@main
struct _6_Loading_an_image_from_a_remote_server_Cupcake_Corner_3_9App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
