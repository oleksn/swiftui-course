//
//  ContentView.swift
//  63 Resizing images to fit the screen using GeometryReader Moonshot 1 11
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

struct ContentView: View {
    var body: some View {
//        Image("Example")
//////        1
////            .frame(width: 300, height: 300)
////            .clipped()
//////        2
//            .resizable()
////            .scaledToFit()
//            .scaledToFill()
//            .frame(width: 300, height: 300)
        

        GeometryReader { geo in
            Image("Example")
                .resizable()
                .scaledToFit()
                .frame(width: geo.size.width * 0.9)
                .frame(width: geo.size.width, height:  geo.size.height)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
