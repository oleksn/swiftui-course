//
//  _3_Resizing_images_to_fit_the_screen_using_GeometryReader_Moonshot_1_11App.swift
//  63 Resizing images to fit the screen using GeometryReader Moonshot 1 11
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _3_Resizing_images_to_fit_the_screen_using_GeometryReader_Moonshot_1_11App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
