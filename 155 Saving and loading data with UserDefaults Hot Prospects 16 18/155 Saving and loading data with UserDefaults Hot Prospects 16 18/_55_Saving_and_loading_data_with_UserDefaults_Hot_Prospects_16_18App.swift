//
//  _55_Saving_and_loading_data_with_UserDefaults_Hot_Prospects_16_18App.swift
//  155 Saving and loading data with UserDefaults Hot Prospects 16 18
//
//  Created by Oleksandr Nesynov on 03.03.2023.
//

import SwiftUI

@main
struct _55_Saving_and_loading_data_with_UserDefaults_Hot_Prospects_16_18App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
