//
//  _1_Hiding_the_keyboard_WeSplit_11_11App.swift
//  11 Hiding the keyboard WeSplit 11 11
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _1_Hiding_the_keyboard_WeSplit_11_11App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
