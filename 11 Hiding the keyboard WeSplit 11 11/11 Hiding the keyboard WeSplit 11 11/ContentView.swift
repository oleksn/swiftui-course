//
//  ContentView.swift
//  11 Hiding the keyboard WeSplit 11 11
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

struct ContentView: View {
    @State var check = 0.0
    @State var number = 2
    @State var tip = 20
    @FocusState var amountInFocused: Bool
    
    let tips = [10, 15, 20, 25, 0]
    var totalPerPerson: Double {
        let peopleCount = Double(number + 2)
        let tipSelection = Double(tip)
        
        let tipValue = check / 100 * tipSelection
        let grantTotal = check + tipValue
        let amountPerPerson = grantTotal / peopleCount
        
        return amountPerPerson
    }
    
    var body: some View {
        NavigationView {
            Form {
                Section {
                    TextField("Amount", value: $check,
                              //                    format: .currency(code: "USD")
                              format: .currency(
                                code: Locale.current.currency?.identifier ?? "USD"
                              )
                    )
                    .keyboardType(.decimalPad)
                    .focused($amountInFocused)
                    
                    Picker("Number of people", selection: $number) {
                        ForEach(2..<100) {
                            Text("\($0) people")
                        }
                    }
                }
                
                Section {
//                    Text("Header")
                    Picker("Percentage", selection: $tip) {
                        ForEach(tips, id: \.self) {
                            Text($0, format: .percent)
                        }
                    }.pickerStyle(.segmented)
                } header: {
                    Text("Header")
                }
                
                Section {
                    Text(totalPerPerson, format: .currency(
                        code: Locale.current.currency?.identifier ?? "USD"
                    ))
                }
            }
        }
        .navigationTitle("navigationTitle")
        .toolbar {
            ToolbarItemGroup(placement: .keyboard) {
                Spacer(
                )
                Button("Done") {
                    amountInFocused = false
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

