//
//  _42_Manually_publishing_ObservableObject_changes_Hot_Prospects_3_18App.swift
//  142 Manually publishing ObservableObject changes Hot Prospects 3 18
//
//  Created by Oleksandr Nesynov on 03.03.2023.
//

import SwiftUI

@main
struct _42_Manually_publishing_ObservableObject_changes_Hot_Prospects_3_18App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
