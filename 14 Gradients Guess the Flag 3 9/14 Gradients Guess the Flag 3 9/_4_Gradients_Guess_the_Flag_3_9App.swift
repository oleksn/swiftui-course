//
//  _4_Gradients_Guess_the_Flag_3_9App.swift
//  14 Gradients Guess the Flag 3 9
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _4_Gradients_Guess_the_Flag_3_9App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
