//
//  ContentView.swift
//  14 Gradients Guess the Flag 3 9
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
////            1)
//            LinearGradient(gradient: Gradient(colors: [.white, .black]), startPoint: .top, endPoint: .bottom)
            
////            2)
//            LinearGradient(gradient: Gradient(stops: [
//                Gradient.Stop(color: .white, location: 0.45),
//                Gradient.Stop(color: .black, location: 0.55)
//            ]), startPoint: .top, endPoint: .bottom)
            
////            3)
//            RadialGradient(gradient: Gradient.init(colors: [Color.blue, .black]), center: UnitPoint.center, startRadius: 20, endRadius: 200)
            
            AngularGradient(gradient: Gradient(colors: [Color.red, .yellow, .green, .blue, .purple, .red]), center: UnitPoint.center)
            
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
