//
//  _6_Connecting_to_Core_ML_BetterRest_6_7App.swift
//  36 Connecting to Core ML BetterRest 6 7
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _6_Connecting_to_Core_ML_BetterRest_6_7App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
