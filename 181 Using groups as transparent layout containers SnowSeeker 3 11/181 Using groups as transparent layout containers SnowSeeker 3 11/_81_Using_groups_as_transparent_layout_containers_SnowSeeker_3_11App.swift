//
//  _81_Using_groups_as_transparent_layout_containers_SnowSeeker_3_11App.swift
//  181 Using groups as transparent layout containers SnowSeeker 3 11
//
//  Created by Oleksandr Nesynov on 10.03.2023.
//

import SwiftUI

@main
struct _81_Using_groups_as_transparent_layout_containers_SnowSeeker_3_11App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
