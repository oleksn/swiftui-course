//
//  ContentView.swift
//  181 Using groups as transparent layout containers SnowSeeker 3 11
//
//  Created by Oleksandr Nesynov on 10.03.2023.
//

import SwiftUI

struct UserView: View {
    var body: some View {
        Group {
            Text("Hello1!")
            Text("Hello2!")
            Text("Hello2!")
        }
        .font(.title)
    }
}

struct ContentView: View {
    @Environment(\.horizontalSizeClass) var sizeClass
    @State var layoutVertically = true
    
    var body: some View {
        //        Group {
        ////          //1)
        //            if layoutVertically {
        //                VStack {
        //                    UserView()
        //                }
        //            } else {
        //                HStack {
        //                    UserView()
        //                }
        //            }
        //        }.onTapGesture {
        //            layoutVertically.toggle()
        //        }
        //    }
            
        if sizeClass == .compact {
            VStack(content: UserView.init)
        } else {
            HStack(content: UserView.init)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
