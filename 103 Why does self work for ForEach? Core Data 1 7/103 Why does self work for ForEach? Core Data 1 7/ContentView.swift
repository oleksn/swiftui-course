//
//  ContentView.swift
//  103 Why does self work for ForEach? Core Data 1 7
//
//  Created by Oleks on 16.02.2023.
//

import SwiftUI

struct Student: Hashable {
    let name: String
    
    
}

struct ContentView: View {
    let students: [Student] = [
        Student(name: "Harry Potter"),
        Student(name: "Hermione Granger")
    ]
    var body: some View {
//        List {
//            ForEach([2, 4, 6, 8, 10], id: \.self) {
//                Text("\($0) is even")
//            }
//        }
        
        List {
            ForEach(students, id: \.self) { student in
                Text(student.name)
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
