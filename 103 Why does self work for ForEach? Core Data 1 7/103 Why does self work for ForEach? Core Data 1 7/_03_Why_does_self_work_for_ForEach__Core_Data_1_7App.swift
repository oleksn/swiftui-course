//
//  _03_Why_does_self_work_for_ForEach__Core_Data_1_7App.swift
//  103 Why does self work for ForEach? Core Data 1 7
//
//  Created by Oleks on 16.02.2023.
//

import SwiftUI

@main
struct _03_Why_does_self_work_for_ForEach__Core_Data_1_7App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
