//
//  _5_Deleting_items_using_onDelete_iExpense_4_11App.swift
//  55 Deleting items using onDelete iExpense 4 11
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _5_Deleting_items_using_onDelete_iExpense_4_11App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
