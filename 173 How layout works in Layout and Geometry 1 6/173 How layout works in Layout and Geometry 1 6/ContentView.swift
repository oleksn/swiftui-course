//
//  ContentView.swift
//  173 How layout works in Layout and Geometry 1 6
//
//  Created by Oleksandr Nesynov on 08.03.2023.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
//            Text("Hello, world!")
//                .padding(20)
//                .background(.red)
            Color.red
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
