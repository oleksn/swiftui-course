//
//  _73_How_layout_works_in_Layout_and_Geometry_1_6App.swift
//  173 How layout works in Layout and Geometry 1 6
//
//  Created by Oleksandr Nesynov on 08.03.2023.
//

import SwiftUI

@main
struct _73_How_layout_works_in_Layout_and_Geometry_1_6App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
