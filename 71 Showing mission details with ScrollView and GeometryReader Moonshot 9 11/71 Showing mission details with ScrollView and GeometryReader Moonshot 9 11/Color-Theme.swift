//
//  Color-Theme.swift
//  71 Showing mission details with ScrollView and GeometryReader Moonshot 9 11
//
//  Created by Oleks on 03.02.2023
//

import Foundation
import SwiftUI

extension ShapeStyle where Self == Color {
    static var darkBackground: Color {
        Color(red: 0.1, green: 0.1, blue: 0.2)
    }

    static var lightBackground: Color {
        Color(red: 0.2, green: 0.2, blue: 0.3)
    }
}
