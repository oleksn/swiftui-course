//
//  _1_Showing_mission_details_with_ScrollView_and_GeometryReader_Moonshot_9_11App.swift
//  71 Showing mission details with ScrollView and GeometryReader Moonshot 9 11
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _1_Showing_mission_details_with_ScrollView_and_GeometryReader_Moonshot_9_11App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
