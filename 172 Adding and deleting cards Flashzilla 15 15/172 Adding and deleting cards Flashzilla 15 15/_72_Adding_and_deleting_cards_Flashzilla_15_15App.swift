//
//  _72_Adding_and_deleting_cards_Flashzilla_15_15App.swift
//  172 Adding and deleting cards Flashzilla 15 15
//
//  Created by Oleksandr Nesynov on 07.03.2023.
//

import SwiftUI

@main
struct _72_Adding_and_deleting_cards_Flashzilla_15_15App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
