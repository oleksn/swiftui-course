//
//  _5_Paths_vs_shapes_in_Drawing_2_10App.swift
//  75 Paths vs shapes in Drawing 2 10
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _5_Paths_vs_shapes_in_Drawing_2_10App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
