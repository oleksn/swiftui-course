//
//  ContentView.swift
//  75 Paths vs shapes in Drawing 2 10
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

struct Triangle: Shape {
    func path(in rect: CGRect) -> Path {
        var path = Path()
        
        path.move(to: CGPoint(x: rect.midX, y: rect.minY))
        path.addLine(to: CGPoint.init(x: rect.minX, y: rect.maxY))
        path.addLine(to: CGPoint.init(x: rect.maxX, y: rect.maxY))
        path.addLine(to: CGPoint.init(x: rect.midX, y: rect.minY))
        
        return path
    }
}

struct Arc: Shape {
    let startAngle: Angle
    let endAngle: Angle
    let clockwise: Bool
    
    func path(in rect: CGRect) -> Path {
        let rotationAdjustment = Angle.degrees(90)
        let modifiedStart = startAngle - rotationAdjustment
        let modifiedEnd = endAngle - rotationAdjustment
        
        var path = Path()
        path.addArc(
            center: CGPoint.init(x: rect.midX, y: rect.midY),
            radius: rect.width / 2,
            startAngle: modifiedStart,
            endAngle: modifiedEnd,
            clockwise: clockwise
        )
        return path
    }
}

struct ContentView: View {
    var body: some View {
////    1)
//        Triangle()
////            .fill(.red)
//            .stroke(.red, style: StrokeStyle(
//                lineWidth: 10,
//                lineCap: CGLineCap.round,
//                lineJoin: CGLineJoin.round
//            ))
//            .frame(width: 300, height: 300)
////    2)
        Arc(startAngle: Angle.degrees(0),
            endAngle: Angle.degrees(110),
            clockwise: true)
        .stroke(.blue, lineWidth: 10)
        .frame(width: 300, height: 300)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
