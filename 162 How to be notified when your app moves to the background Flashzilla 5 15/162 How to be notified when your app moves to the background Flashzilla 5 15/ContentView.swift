//
//  ContentView.swift
//  162 How to be notified when your app moves to the background Flashzilla 5 15
//
//  Created by Oleksandr Nesynov on 03.03.2023.
//

import SwiftUI

struct ContentView: View {
    @Environment(\.scenePhase) var scenePhase
    
    var body: some View {
        VStack {
            Image(systemName: "globe")
                .imageScale(.large)
                .foregroundColor(.accentColor)
            Text("Hello, world!")
        }
        .padding()
        .onChange(of: scenePhase) { newValue in
            if newValue == .active {
                print("Active")
            } else if newValue == .inactive {
                print("Inactive")
            } else if newValue == .background {
                print("Background")
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
