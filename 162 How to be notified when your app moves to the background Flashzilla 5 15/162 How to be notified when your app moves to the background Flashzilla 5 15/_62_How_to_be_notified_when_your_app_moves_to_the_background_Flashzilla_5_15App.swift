//
//  _62_How_to_be_notified_when_your_app_moves_to_the_background_Flashzilla_5_15App.swift
//  162 How to be notified when your app moves to the background Flashzilla 5 15
//
//  Created by Oleksandr Nesynov on 03.03.2023.
//

import SwiftUI

@main
struct _62_How_to_be_notified_when_your_app_moves_to_the_background_Flashzilla_5_15App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
