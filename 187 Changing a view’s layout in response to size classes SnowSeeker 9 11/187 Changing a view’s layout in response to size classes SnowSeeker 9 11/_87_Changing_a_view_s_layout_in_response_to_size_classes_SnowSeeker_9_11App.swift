//
//  _87_Changing_a_view_s_layout_in_response_to_size_classes_SnowSeeker_9_11App.swift
//  187 Changing a view’s layout in response to size classes SnowSeeker 9 11
//
//  Created by Oleksandr Nesynov on 10.03.2023.
//

import SwiftUI

@main
struct _87_Changing_a_view_s_layout_in_response_to_size_classes_SnowSeeker_9_11App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
