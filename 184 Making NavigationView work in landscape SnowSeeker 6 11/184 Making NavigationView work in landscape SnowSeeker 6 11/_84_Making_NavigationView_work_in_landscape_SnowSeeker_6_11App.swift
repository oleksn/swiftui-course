//
//  _84_Making_NavigationView_work_in_landscape_SnowSeeker_6_11App.swift
//  184 Making NavigationView work in landscape SnowSeeker 6 11
//
//  Created by Oleksandr Nesynov on 10.03.2023.
//

import SwiftUI

@main
struct _84_Making_NavigationView_work_in_landscape_SnowSeeker_6_11App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
