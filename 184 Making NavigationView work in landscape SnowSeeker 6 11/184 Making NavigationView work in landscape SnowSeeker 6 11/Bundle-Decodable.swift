//
//  Bundle-Decodable.swift
//  183 Building a primary list of items SnowSeeker 5 11
//
//  Created by Oleksandr Nesynov on 10.03.2023.
//

import Foundation

extension Bundle {
    func decode<T: Decodable>(_ file: String) -> T {
        guard let url = self.url(forResource: file, withExtension: nil) else {
            fatalError("Failed to locate \(file) in bundle")
        }
        
        guard let data = try? Data(contentsOf: url) else {
            fatalError("Failed to load \(file) from bundle.")
        }
        
        let decode = JSONDecoder()
        
        guard let loaded = try? decode.decode(T.self, from: data) else {
            fatalError("Failed to decode \(file) from bundle.")
        }
        
        return loaded
    }
}
