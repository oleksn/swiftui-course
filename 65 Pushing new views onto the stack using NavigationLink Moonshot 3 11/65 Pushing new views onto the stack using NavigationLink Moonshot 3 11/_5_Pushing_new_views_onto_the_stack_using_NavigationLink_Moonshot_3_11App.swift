//
//  _5_Pushing_new_views_onto_the_stack_using_NavigationLink_Moonshot_3_11App.swift
//  65 Pushing new views onto the stack using NavigationLink Moonshot 3 11
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _5_Pushing_new_views_onto_the_stack_using_NavigationLink_Moonshot_3_11App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
