//
//  ContentView.swift
//  65 Pushing new views onto the stack using NavigationLink Moonshot 3 11
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        //// 1)
//        NavigationView {
//            NavigationLink {
//                Text("Detail View")
//            } label: {
//                Text("Hello, world!")
//                    .padding()
//            }
//            .navigationTitle("SwiftUI")
//        }
        //// 2)
        NavigationView {
            List(0..<100) { row in
                
                NavigationLink {
                    Text("Detail \(row)")
                } label: {
                    Text("Row \(row)")
                }
                .navigationTitle("SwiftUI")
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
