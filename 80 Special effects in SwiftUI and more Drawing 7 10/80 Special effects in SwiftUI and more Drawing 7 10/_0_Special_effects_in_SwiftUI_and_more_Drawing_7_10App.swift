//
//  _0_Special_effects_in_SwiftUI_and_more_Drawing_7_10App.swift
//  80 Special effects in SwiftUI and more Drawing 7 10
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _0_Special_effects_in_SwiftUI_and_more_Drawing_7_10App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
