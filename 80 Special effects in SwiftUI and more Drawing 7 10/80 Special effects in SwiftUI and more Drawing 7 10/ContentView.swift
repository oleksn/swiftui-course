//
//  ContentView.swift
//  80 Special effects in SwiftUI and more Drawing 7 10
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

struct ContentView: View {
    @State var amount = 0.0
    
    var body: some View {
////        1) TYPE
//        ZStack {
//            Image("PaulHudson")
//                .colorMultiply(.red)
//
////            Rectangle()
////                .fill(.red)
////                .blendMode(.multiply)
//        }
////        2) TYPE
//        VStack {
//            ZStack {
//                Circle()
//                    .fill(.red)
//                    .frame(width: 200 * amount)
//                    .offset(x: -50, y: -80)
//                    .blendMode(.screen)
//
//                Circle()
//                    .fill(.green)
//                    .frame(width: 200 * amount)
//                    .offset(x: 50, y: -80)
//                    .blendMode(.screen)
//
//                Circle()
//                    .fill(.blue)
//                    .frame(width: 200 * amount)
//                    .blendMode(.screen)
//            }
//            .frame(width: 300, height: 300)
        
////        3) TYPE
        VStack {
        Image("PaulHudson")
            .resizable()
            .scaledToFit()
            .frame(width: 200, height: 200)
            .saturation(amount)
            .blur(radius: (1 - amount) * 20)
            
            Slider(value: $amount)
                .padding()
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(.black)
        .ignoresSafeArea()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
