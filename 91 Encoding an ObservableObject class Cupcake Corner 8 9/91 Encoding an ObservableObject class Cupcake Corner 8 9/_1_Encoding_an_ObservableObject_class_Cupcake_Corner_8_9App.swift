//
//  _1_Encoding_an_ObservableObject_class_Cupcake_Corner_8_9App.swift
//  91 Encoding an ObservableObject class Cupcake Corner 8 9
//
//  Created by Oleks on 09.02.2023.
//

import SwiftUI

@main
struct _1_Encoding_an_ObservableObject_class_Cupcake_Corner_8_9App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
