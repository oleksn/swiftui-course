//
//  _2_Why__State_only_works_with_structs_iExpense_1_11App.swift
//  52 Why @State only works with structs iExpense 1 11
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _2_Why__State_only_works_with_structs_iExpense_1_11App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
