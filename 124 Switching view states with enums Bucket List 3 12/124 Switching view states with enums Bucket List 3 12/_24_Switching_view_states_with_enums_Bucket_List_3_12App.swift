//
//  _24_Switching_view_states_with_enums_Bucket_List_3_12App.swift
//  124 Switching view states with enums Bucket List 3 12
//
//  Created by Oleksandr Nesynov on 22.02.2023.
//

import SwiftUI

@main
struct _24_Switching_view_states_with_enums_Bucket_List_3_12App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
