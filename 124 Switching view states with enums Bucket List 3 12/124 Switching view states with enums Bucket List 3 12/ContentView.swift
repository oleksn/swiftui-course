//
//  ContentView.swift
//  124 Switching view states with enums Bucket List 3 12
//
//  Created by Oleksandr Nesynov on 22.02.2023.
//

import SwiftUI

enum LoadingState {
    case loading, success, failed
}

struct LoadingView: View {
    var body: some View {
        Text("Loading...")
    }
}

struct SuccessView: View {
    var body: some View {
        Text("Success!")
    }
}

struct FailedView: View {
    var body: some View {
        Text("Failed.")
    }
}

struct ContentView: View {
    var loadingState = LoadingState.loading

    var body: some View {
//        if Bool.random() {
//            Rectangle()
//        } else {
//            Circle()
//        }
        
//        if loadingState == .loading {
//            LoadingView()
//        } else if loadingState == .success {
//            SuccessView()
//        } else if loadingState == .failed {
//            FailedView()
//        }
        
        switch loadingState {
        case .loading: LoadingView()
        case .success: SuccessView()
        case .failed: FailedView()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
