//
//  _3_Working_with_dates_BetterRest_3_7App.swift
//  33 Working with dates BetterRest 3 7
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _3_Working_with_dates_BetterRest_3_7App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
