//
//  ContentView.swift
//  33 Working with dates BetterRest 3 7
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
////            4)
//            Text(
//                Date.now,
//                format: .dateTime.day().month().year()
//            )
            
            Text(Date.now.formatted(
                date: Date.FormatStyle.DateStyle.long,
////            5)
//                time: Date.FormatStyle.TimeStyle.shortened
                time: Date.FormatStyle.TimeStyle.omitted
            ))
        }
    }
    
    func trivialExample() {
////        1)
//        let now = Date.now
//        let tomorrow = Date.now.addingTimeInterval(86400)
//        let range = now...tomorrow
////        2)
//        var components = DateComponents()
//        components.hour = 8
//        components.minute = 0
//
//        let date = Calendar.current.date(from: components)
////        3)
//        let components = Calendar.current.dateComponents([.hour, .minute], from: Date.now)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
