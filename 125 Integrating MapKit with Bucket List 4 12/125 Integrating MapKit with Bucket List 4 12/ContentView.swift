//
//  ContentView.swift
//  125 Integrating MapKit with Bucket List 4 12
//
//  Created by Oleksandr Nesynov on 22.02.2023.
//

import MapKit
import SwiftUI

struct Location: Identifiable {
    let id = UUID()
    let name : String
    let coordinate: CLLocationCoordinate2D
}

struct ContentView: View {
    @State var mapRegion = MKCoordinateRegion(
        center: CLLocationCoordinate2D(
            latitude: 51.5,
            longitude: -0.12
        ), span: MKCoordinateSpan(
            latitudeDelta: 0.2,
            longitudeDelta: 0.2
        ))
    
    let locations = [
        Location(
            name: "Buckingham Palace",
            coordinate: CLLocationCoordinate2D(
                latitude: 51.501,
                longitude: -0.141
            )
        ),
        Location(
            name: "Tower of London",
            coordinate: CLLocationCoordinate2D(
                latitude: 51.508,
                longitude: -0.076
            )
        )
    ]
    
    var body: some View {
////                     4)
//        Map(
//            coordinateRegion: $mapRegion,
//            annotationItems: locations) { location in
////                MapMarker(coordinate: location.coordinate)
//                MapAnnotation(coordinate: location.coordinate) {
//////                     1)
////                    Circle()
////                        .stroke(.red, lineWidth: 3)
////                        .frame(width: 44, height: 44)
//////                     2)
////                    VStack {
////                        Circle()
////                            .stroke(.red, lineWidth: 3)
////                            .frame(width: 44, height: 44)
////
////                        Text(location.name)
////                    }
//////                     3)
//                    Circle()
//                        .stroke(.red, lineWidth: 3)
//                        .frame(width: 44, height: 44)
//                        .onTapGesture {
//                            print("Tapped on \(location.name)")
//                        }
//                }
//            }
////                     4)
        NavigationView {
            Map(
                coordinateRegion: $mapRegion,
                annotationItems: locations
            ) { location in
                    MapAnnotation(coordinate: location.coordinate) {
                        NavigationLink() {
                            Text(location.name)
                        } label: {
                            Circle()
                                .stroke(.red, lineWidth: 3)
                                .frame(width: 44, height: 44)
                        }
                    }
            }
            .navigationTitle("London Explorer")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
