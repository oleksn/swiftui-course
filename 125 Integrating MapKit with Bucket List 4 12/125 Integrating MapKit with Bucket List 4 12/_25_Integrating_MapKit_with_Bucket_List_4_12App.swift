//
//  _25_Integrating_MapKit_with_Bucket_List_4_12App.swift
//  125 Integrating MapKit with Bucket List 4 12
//
//  Created by Oleksandr Nesynov on 22.02.2023.
//

import SwiftUI

@main
struct _25_Integrating_MapKit_with_Bucket_List_4_12App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
