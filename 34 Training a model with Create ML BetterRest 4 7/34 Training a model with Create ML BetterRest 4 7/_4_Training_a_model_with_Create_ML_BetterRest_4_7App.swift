//
//  _4_Training_a_model_with_Create_ML_BetterRest_4_7App.swift
//  34 Training a model with Create ML BetterRest 4 7
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _4_Training_a_model_with_Create_ML_BetterRest_4_7App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
