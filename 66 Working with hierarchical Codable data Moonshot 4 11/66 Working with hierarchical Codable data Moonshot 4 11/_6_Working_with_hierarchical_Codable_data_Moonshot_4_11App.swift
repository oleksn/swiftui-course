//
//  _6_Working_with_hierarchical_Codable_data_Moonshot_4_11App.swift
//  66 Working with hierarchical Codable data Moonshot 4 11
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _6_Working_with_hierarchical_Codable_data_Moonshot_4_11App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
