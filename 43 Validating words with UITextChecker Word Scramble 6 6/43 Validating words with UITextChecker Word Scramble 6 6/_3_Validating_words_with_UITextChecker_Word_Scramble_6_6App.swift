//
//  _3_Validating_words_with_UITextChecker_Word_Scramble_6_6App.swift
//  43 Validating words with UITextChecker Word Scramble 6 6
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _3_Validating_words_with_UITextChecker_Word_Scramble_6_6App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
