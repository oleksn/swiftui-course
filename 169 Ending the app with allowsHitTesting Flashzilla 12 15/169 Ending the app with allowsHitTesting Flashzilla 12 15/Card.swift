//
//  Card.swift
//  164 Designing a single card view Flashzilla 7 15
//
//  Created by Oleksandr Nesynov on 03.03.2023.
//

import Foundation

struct Card: Codable {
    let prompt: String
    let answer: String

    static let example = Card(prompt: "Who played the 13th Doctor in Doctor Who?", answer: "Jodie Whittaker")
}
