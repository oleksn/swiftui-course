//
//  _69_Ending_the_app_with_allowsHitTesting_Flashzilla_12_15App.swift
//  169 Ending the app with allowsHitTesting Flashzilla 12 15
//
//  Created by Oleksandr Nesynov on 06.03.2023.
//

import SwiftUI

@main
struct _69_Ending_the_app_with_allowsHitTesting_Flashzilla_12_15App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
