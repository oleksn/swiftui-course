//
//  ContentView.swift
//  28 View composition Views and Modifiers 8 10
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

struct CapsuleText: View {
    var text: String
    
    var body: some View {
        Text(text)
            .font(.largeTitle)
            .padding()
//            .foregroundColor(.white)
            .background(.blue)
            .clipShape(Capsule())
    }
}

struct ContentView: View {
    var body: some View {
        VStack(spacing: 10) {
////         1)
//            Text("First")
//                .font(.largeTitle)
//                .padding()
//                .foregroundColor(.white)
//                .background(.blue)
//                .clipShape(Capsule())
//
//            Text("Second")
//                .font(.largeTitle)
//                .padding()
//                .foregroundColor(.white)
//                .background(.blue)
//                .clipShape(Capsule())
            
            CapsuleText(text: "First")
                .foregroundColor(.white)
            CapsuleText(text: "Second")
                .foregroundColor(.yellow)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
