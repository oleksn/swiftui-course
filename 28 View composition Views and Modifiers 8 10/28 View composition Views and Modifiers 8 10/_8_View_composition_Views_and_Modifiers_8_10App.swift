//
//  _8_View_composition_Views_and_Modifiers_8_10App.swift
//  28 View composition Views and Modifiers 8 10
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _8_View_composition_Views_and_Modifiers_8_10App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
