//
//  ImageSaver.swift
//  121 Saving the filtered image with UIImageWriteToSavedPhotosAlbum Instafilter 12 12
//
//  Created by Oleksandr Nesynov on 22.02.2023.
//

import UIKit

class ImageSaver: NSObject {
    var successHandler: (() -> Void)?
    var errorHandler: ((Error) -> Void)?
    
    func writToPhotoAlbum(image: UIImage) {
        UIImageWriteToSavedPhotosAlbum(
            image,
            self,
            #selector(saveCompleted),
            nil
        )
    }
    
    @objc func saveCompleted(
        _ image: UIImage,
        didFinishSavingWithError error: Error?,
        contextInfo: UnsafeRawPointer
    ) {
        if let error = error {
            errorHandler?(error)
        } else {
            print("Save finished!")
            successHandler?()
        }
    }
}
