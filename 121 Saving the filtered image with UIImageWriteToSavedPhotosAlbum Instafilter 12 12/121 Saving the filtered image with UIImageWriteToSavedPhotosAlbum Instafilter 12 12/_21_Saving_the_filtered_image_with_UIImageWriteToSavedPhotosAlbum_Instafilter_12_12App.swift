//
//  _21_Saving_the_filtered_image_with_UIImageWriteToSavedPhotosAlbum_Instafilter_12_12App.swift
//  121 Saving the filtered image with UIImageWriteToSavedPhotosAlbum Instafilter 12 12
//
//  Created by Oleksandr Nesynov on 22.02.2023.
//

import SwiftUI

@main
struct _21_Saving_the_filtered_image_with_UIImageWriteToSavedPhotosAlbum_Instafilter_12_12App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
