//
//  _7_Cleaning_up_the_user_interface_BetterRest_7_7App.swift
//  37 Cleaning up the user interface BetterRest 7 7
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _7_Cleaning_up_the_user_interface_BetterRest_7_7App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
