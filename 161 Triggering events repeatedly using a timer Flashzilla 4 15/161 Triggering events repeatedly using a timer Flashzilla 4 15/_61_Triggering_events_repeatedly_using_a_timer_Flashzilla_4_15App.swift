//
//  _61_Triggering_events_repeatedly_using_a_timer_Flashzilla_4_15App.swift
//  161 Triggering events repeatedly using a timer Flashzilla 4 15
//
//  Created by Oleksandr Nesynov on 03.03.2023.
//

import SwiftUI

@main
struct _61_Triggering_events_repeatedly_using_a_timer_Flashzilla_4_15App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
