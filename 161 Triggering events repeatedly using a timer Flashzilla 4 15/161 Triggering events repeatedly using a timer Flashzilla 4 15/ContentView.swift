//
//  ContentView.swift
//  161 Triggering events repeatedly using a timer Flashzilla 4 15
//
//  Created by Oleksandr Nesynov on 03.03.2023.
//

import SwiftUI

struct ContentView: View {
    let timer = Timer.publish(
        every: 1,
        on: .main,
        in: .common
    ).autoconnect()
    
    @State var counter = 0
    
    var body: some View {
        Text("Hello, world!")
            .onReceive(timer) { time in
                if counter == 5 {
                    timer
                        .upstream
                        .connect()
                        .cancel()
                } else {
                    print("Now \(time)!")
                }
                
                counter += 1
            }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
