//
//  _32_Introducing_MVVM_into_your_project_Bucket_List_11_12App.swift
//  132 Introducing MVVM into your project Bucket List 11 12
//
//  Created by Oleksandr Nesynov on 01.03.2023.
//

import SwiftUI

@main
struct _32_Introducing_MVVM_into_your_project_Bucket_List_11_12App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
