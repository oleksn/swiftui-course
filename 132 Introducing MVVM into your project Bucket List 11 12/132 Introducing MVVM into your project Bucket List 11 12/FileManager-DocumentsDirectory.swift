//
//  FileManager-DocumentsDirectory.swift
//  132 Introducing MVVM into your project Bucket List 11 12
//
//  Created by Oleksandr Nesynov on 01.03.2023.
//

import Foundation

extension FileManager {
    static var documentsDirectory: URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
}
