//
//  _9_Checking_for_a_valid_address_Cupcake_Corner_6_9App.swift
//  89 Checking for a valid address Cupcake Corner 6 9
//
//  Created by Oleks on 09.02.2023.
//

import SwiftUI

@main
struct _9_Checking_for_a_valid_address_Cupcake_Corner_6_9App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
