//
//  CheckoutView.swift
//  89 Checking for a valid address Cupcake Corner 6 9
//
//  Created by Oleks on 09.02.2023.
//

import SwiftUI

struct CheckoutView: View {
    @ObservedObject var order: Order
    
    @State var confirmationMessage = ""
    @State var showingConfirmation = false
    
    var body: some View {
//        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
        ScrollView {
            VStack {
                AsyncImage(
                    url: URL(string: "https://hws.dev/img/cupcakes@3x.jpg"),
                    scale: 3
                ) { image in
                    image
                        .resizable()
                        .scaledToFit()
                } placeholder: {
                    ProgressView()
                }
                .frame(height: 233)
                
//                Text("Your total is \(order.cost, format: .сurrency(code: "USD"))")
                Text("Your total is \(order.cost)")
                    .font(.title)
                
                Button("Place Order") {
                    Task {
                        await placeOrder()
                    }
                }
                .padding()
            }
        }
        .navigationTitle("Check iut")
        .navigationBarTitleDisplayMode(.inline)
        .alert(
            "Thank you!",
            isPresented: $showingConfirmation) {
                Button("OK") {
                    
                }
            } message: {
                Text(confirmationMessage)
            }
    }
    
    func placeOrder() async {
        guard let ordered = try? JSONEncoder().encode(order) else {
            print("Failed to encode order")
            return
        }
        
        let url = URL(string: "https://reqres.in/api/cupcakes")!
        var request = URLRequest(url: url)
        request.setValue(
            "application/json",
            forHTTPHeaderField: "Content-Type"
        )
        request.httpMethod = "POST"
        
        do {
            let (data, _) = try await URLSession.shared.upload(
                for: request,
                from: ordered
            )
            let decodedOrder = try JSONDecoder().decode(
                Order.self,
                from: data
            )
            confirmationMessage = "Your order for \(decodedOrder.quantity)x \(Order.types[decodedOrder.type].lowercased()) cupcakes is on its way!"
            showingConfirmation = true
            
        } catch {
            print("Checkout failed.")
        }
    }
}

struct CheckoutView_Previews: PreviewProvider {
    static var previews: some View {
        CheckoutView(order: Order())
    }
}
