//
//  _38_Fixing_Word_Scramble_Accessibility_5_6App.swift
//  138 Fixing Word Scramble Accessibility 5 6
//
//  Created by Oleksandr Nesynov on 02.03.2023.
//

import SwiftUI

@main
struct _38_Fixing_Word_Scramble_Accessibility_5_6App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
