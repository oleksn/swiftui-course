//
//  _70_Making_iPhones_vibrate_with_UINotificationFeedbackGenerator_Flashzilla_13_15App.swift
//  170 Making iPhones vibrate with UINotificationFeedbackGenerator Flashzilla 13 15
//
//  Created by Oleksandr Nesynov on 06.03.2023.
//

import SwiftUI

@main
struct _70_Making_iPhones_vibrate_with_UINotificationFeedbackGenerator_Flashzilla_13_15App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
