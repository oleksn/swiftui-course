//
//  _22_Adding_conformance_to_Comparable_for_custom_types_Bucket_List_1_12App.swift
//  122 Adding conformance to Comparable for custom types Bucket List 1 12
//
//  Created by Oleksandr Nesynov on 22.02.2023.
//

import SwiftUI

@main
struct _22_Adding_conformance_to_Comparable_for_custom_types_Bucket_List_1_12App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
