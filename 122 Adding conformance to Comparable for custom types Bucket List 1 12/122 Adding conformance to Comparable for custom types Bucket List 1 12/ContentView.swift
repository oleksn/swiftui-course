//
//  ContentView.swift
//  122 Adding conformance to Comparable for custom types Bucket List 1 12
//
//  Created by Oleksandr Nesynov on 22.02.2023.
//

import SwiftUI

struct User: Identifiable, Comparable {
    let id = UUID()
    let firstName: String
    let lastName: String
    
    static func <(lhs: User, rhs: User) -> Bool {
        lhs.lastName < rhs.lastName
    }
}

struct ContentView: View {
//    let values = [1, 5, 3, 6, 2, 9]
    
    let users = [
        User(firstName: "Arnold", lastName: "Rimmer"),
        User(firstName: "Kristine", lastName: "Kochanski"),
        User(firstName: "David", lastName: "List")
    ]
//    .sorted {
//        $0.lastName < $1.lastName
//    }
    .sorted()
    
    var body: some View {
//        List(values, id: \.self) {
        List(users)  { user in
            Text("\(user.firstName) \(user.lastName)")
        }
    }
    
//    func example() {
//        let result = 4 < 5
//    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
