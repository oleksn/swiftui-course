//
//  _19_Basic_image_filtering_using_Core_Image_Instafilter_10_12App.swift
//  119 Basic image filtering using Core Image Instafilter 10 12
//
//  Created by Oleks on 17.02.2023.
//

import SwiftUI

@main
struct _19_Basic_image_filtering_using_Core_Image_Instafilter_10_12App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
