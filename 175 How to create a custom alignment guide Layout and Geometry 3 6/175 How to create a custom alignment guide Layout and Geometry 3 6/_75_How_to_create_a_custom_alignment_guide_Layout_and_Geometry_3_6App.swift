//
//  _75_How_to_create_a_custom_alignment_guide_Layout_and_Geometry_3_6App.swift
//  175 How to create a custom alignment guide Layout and Geometry 3 6
//
//  Created by Oleksandr Nesynov on 10.03.2023.
//

import SwiftUI

@main
struct _75_How_to_create_a_custom_alignment_guide_Layout_and_Geometry_3_6App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
