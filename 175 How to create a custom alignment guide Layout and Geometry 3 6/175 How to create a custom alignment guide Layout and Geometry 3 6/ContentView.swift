//
//  ContentView.swift
//  175 How to create a custom alignment guide Layout and Geometry 3 6
//
//  Created by Oleksandr Nesynov on 10.03.2023.
//

import SwiftUI

extension VerticalAlignment {
    enum MidAccountAndName: AlignmentID {
        static func defaultValue(in context: ViewDimensions) -> CGFloat {
            context[.top]
        }
    }
    
    static let midAccountAndName = VerticalAlignment(MidAccountAndName.self)
}

struct ContentView: View {
    var body: some View {
        HStack(alignment: .midAccountAndName) {
            VStack {
                Text("@twostraws")
                    .alignmentGuide(.midAccountAndName
                    ) { d in
                        d[VerticalAlignment.center]
                    }
                Image("paul-hudson")
                    .resizable()
                    .frame(
                        width: 64,
                        height: 64
                    )
            }
            
            VStack {
                Text("Full Name:")
                Text("PAUL HUNDSON")
                    .alignmentGuide(.midAccountAndName
                    ) { d in
                        d[VerticalAlignment.center]
                    }
                    .font(.largeTitle)
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
