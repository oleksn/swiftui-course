//
//  _0_Preparing_for_checkout_Cupcake_Corner_7_9App.swift
//  90 Preparing for checkout Cupcake Corner 7 9
//
//  Created by Oleks on 09.02.2023.
//

import SwiftUI

@main
struct _0_Preparing_for_checkout_Cupcake_Corner_7_9App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
