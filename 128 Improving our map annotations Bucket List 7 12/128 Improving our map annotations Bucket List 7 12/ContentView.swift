//
//  ContentView.swift
//  128 Improving our map annotations Bucket List 7 12
//
//  Created by Oleksandr Nesynov on 23.02.2023.
//

import MapKit
import SwiftUI

struct ContentView: View {
    @State var mapRegion = MKCoordinateRegion(
        center: CLLocationCoordinate2D(
            latitude: 50,
            longitude: 0
        ),
        span: MKCoordinateSpan(
            latitudeDelta: 25,
            longitudeDelta: 25
        )
    )
    @State var locations = [Location]()
    
    var body: some View {
        ZStack {
            Map(coordinateRegion: $mapRegion,
                annotationItems: locations) { location in
//                MapMarker(
                MapAnnotation(
                    coordinate: CLLocationCoordinate2D(
                        latitude: location.latitude,
                        longitude: location.longitude
                    )) {
                        VStack {
                            Image(systemName: "star.circle")
                                .resizable()
                                .foregroundColor(.red)
                                .frame(width: 44, height: 44)
                                .background(.white)
                                .clipShape(Circle())
                            
                            Text(location.name)
                        }
                    }
            }
                .ignoresSafeArea()
            
            Circle()
                .fill(.blue)
                .opacity(0.3)
                .frame(width: 32, height: 32)
            
            VStack {
                Spacer()
                
                HStack {
                    Spacer()
                    
                    Button {
                        // create a new location
                        let newLocation = Location(
                            id: UUID(),
                            name: "New location",
                            description: "",
                            latitude: mapRegion.center.latitude,
                            longitude: mapRegion.center.longitude
                        )
                        locations.append(newLocation)
                    } label: {
                        Image(systemName: "plus")
                    }
                    .padding()
                    .background(.black.opacity(075))
                    .foregroundColor(.white)
                    .font(.title)
                    .clipShape(Circle())
                    .padding(.trailing)
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
