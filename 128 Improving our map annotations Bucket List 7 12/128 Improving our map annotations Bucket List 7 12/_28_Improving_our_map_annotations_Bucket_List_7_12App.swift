//
//  _28_Improving_our_map_annotations_Bucket_List_7_12App.swift
//  128 Improving our map annotations Bucket List 7 12
//
//  Created by Oleksandr Nesynov on 23.02.2023.
//

import SwiftUI

@main
struct _28_Improving_our_map_annotations_Bucket_List_7_12App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
