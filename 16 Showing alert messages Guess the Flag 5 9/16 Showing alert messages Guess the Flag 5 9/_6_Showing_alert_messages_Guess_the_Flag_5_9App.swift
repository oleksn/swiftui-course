//
//  _6_Showing_alert_messages_Guess_the_Flag_5_9App.swift
//  16 Showing alert messages Guess the Flag 5 9
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _6_Showing_alert_messages_Guess_the_Flag_5_9App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
