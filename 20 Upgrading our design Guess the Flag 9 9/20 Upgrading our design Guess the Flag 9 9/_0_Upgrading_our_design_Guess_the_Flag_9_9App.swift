//
//  _0_Upgrading_our_design_Guess_the_Flag_9_9App.swift
//  20 Upgrading our design Guess the Flag 9 9
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _0_Upgrading_our_design_Guess_the_Flag_9_9App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
