//
//  _78_ScrollView_effects_using_GeometryReader_Layout_and_Geometry_6_6App.swift
//  178 ScrollView effects using GeometryReader Layout and Geometry 6 6
//
//  Created by Oleksandr Nesynov on 10.03.2023.
//

import SwiftUI

@main
struct _78_ScrollView_effects_using_GeometryReader_Layout_and_Geometry_6_6App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
