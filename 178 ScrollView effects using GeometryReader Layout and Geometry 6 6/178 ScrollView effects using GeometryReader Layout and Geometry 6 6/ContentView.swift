//
//  ContentView.swift
//  178 ScrollView effects using GeometryReader Layout and Geometry 6 6
//
//  Created by Oleksandr Nesynov on 10.03.2023.
//

import SwiftUI

struct ContentView: View {
    let colors: [Color] = [
        .red, .green, .blue, .orange, .pink, .purple, .yellow
    ]
    
    
    var body: some View {
////        1) effect
//        GeometryReader { fullView in
//            ScrollView {
//                ForEach(0..<50) { index in
//                    GeometryReader { geo in
//                        Text("Row #\(index)")
//                            .font(.title)
//                            .frame(maxWidth: .infinity)
//                            .background(colors[index % 7])
//                            .rotation3DEffect(
//                                .degrees(geo.frame(in: .global).minY - fullView.size.height / 2) / 5,
//                                axis: (x: 0, y: 1, z: 0)
//                            )
//                    }.frame(height: 40)
//                }
//            }
//        }
        
////        2) effect
        ScrollView(.horizontal, showsIndicators: false) {
            HStack(spacing: 0) {
                ForEach(0..<20) { number in
                    GeometryReader { geo in
                        Text("Number \(number)")
                            .font(.largeTitle)
                            .padding()
                            .background(.red)
                            .rotation3DEffect(
                                .degrees(-geo.frame(in: .global).minX) / 8,
                                axis: (x: 0, y: 1, z: 0)
                            )
                    }
                    .frame(width: 200, height: 200)
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
