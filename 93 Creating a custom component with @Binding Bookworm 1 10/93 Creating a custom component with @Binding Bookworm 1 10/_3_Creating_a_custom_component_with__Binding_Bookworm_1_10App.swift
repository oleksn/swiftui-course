//
//  _3_Creating_a_custom_component_with__Binding_Bookworm_1_10App.swift
//  93 Creating a custom component with @Binding Bookworm 1 10
//
//  Created by Oleks on 16.02.2023.
//

import SwiftUI

@main
struct _3_Creating_a_custom_component_with__Binding_Bookworm_1_10App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
