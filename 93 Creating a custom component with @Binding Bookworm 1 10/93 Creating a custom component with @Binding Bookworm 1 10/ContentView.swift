//
//  ContentView.swift
//  93 Creating a custom component with @Binding Bookworm 1 10
//
//  Created by Oleks on 16.02.2023.
//

import SwiftUI

struct PushButton: View {
    let title: String
    @Binding var isOn: Bool
    
    var onColor = [Color.red, Color.yellow]
    var offColors = [Color(white: 0.6), Color(white: 0.4)]
    
    var body: some View {
        Button(title) {
            isOn.toggle()
        }
        .padding()
        .background(
            LinearGradient(colors: isOn ? onColor : offColors, startPoint: UnitPoint.top, endPoint: UnitPoint.bottom)
                .foregroundColor(.white)
                .clipShape(Capsule())
                .shadow(radius: isOn ? 0 : 5)
        )
    }
}

struct ContentView: View {
    @State var rememberMe = false
    
    var body: some View {
        VStack {
            PushButton(title: "Remember Me", isOn: $rememberMe)
            Text(rememberMe ? "On" : "Off")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
