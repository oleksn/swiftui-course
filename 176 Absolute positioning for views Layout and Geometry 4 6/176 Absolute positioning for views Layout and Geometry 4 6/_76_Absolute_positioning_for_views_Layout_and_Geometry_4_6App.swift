//
//  _76_Absolute_positioning_for_views_Layout_and_Geometry_4_6App.swift
//  176 Absolute positioning for views Layout and Geometry 4 6
//
//  Created by Oleksandr Nesynov on 10.03.2023.
//

import SwiftUI

@main
struct _76_Absolute_positioning_for_views_Layout_and_Geometry_4_6App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
