//
//  ContentView.swift
//  176 Absolute positioning for views Layout and Geometry 4 6
//
//  Created by Oleksandr Nesynov on 10.03.2023.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
//            .background(.red)
            .position(x: 100, y: 100)
//            .offset(x: 100, y: 100)
            .background(.red)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
