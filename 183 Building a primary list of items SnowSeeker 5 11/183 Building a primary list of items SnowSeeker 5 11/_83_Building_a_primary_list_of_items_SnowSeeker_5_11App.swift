//
//  _83_Building_a_primary_list_of_items_SnowSeeker_5_11App.swift
//  183 Building a primary list of items SnowSeeker 5 11
//
//  Created by Oleksandr Nesynov on 10.03.2023.
//

import SwiftUI

@main
struct _83_Building_a_primary_list_of_items_SnowSeeker_5_11App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
