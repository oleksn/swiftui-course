//
//  Resort.swift
//  183 Building a primary list of items SnowSeeker 5 11
//
//  Created by Oleksandr Nesynov on 10.03.2023.
//

import Foundation

struct Resort: Codable, Identifiable {
    let id: String
    let name: String
    let country: String
    let description: String
    let imageCredit: String
    let price: Int
    let size: Int
    let snowDepth: Int
    let elevation: Int
    let runs: Int
    let facilities: [String]  
    
    var facilityTypes: [Facility] {
        facilities.map(Facility.init)
    }
    
    static let allResorts: [Resort] = Bundle.main.decode("resort.json")
    static let example = allResorts[0]
//    static let example = (
//        Bundle.main.decode("resort.json") as [Resort]
//    )[0]
}
