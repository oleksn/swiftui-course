//
//  _88_Binding_an_alert_to_an_optional_string_SnowSeeker_10_11App.swift
//  188 Binding an alert to an optional string SnowSeeker 10 11
//
//  Created by Oleksandr Nesynov on 10.03.2023.
//

import SwiftUI

@main
struct _88_Binding_an_alert_to_an_optional_string_SnowSeeker_10_11App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
