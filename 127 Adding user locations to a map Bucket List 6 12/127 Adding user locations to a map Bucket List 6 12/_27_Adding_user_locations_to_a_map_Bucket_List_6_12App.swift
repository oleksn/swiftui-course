//
//  _27_Adding_user_locations_to_a_map_Bucket_List_6_12App.swift
//  127 Adding user locations to a map Bucket List 6 12
//
//  Created by Oleksandr Nesynov on 23.02.2023.
//

import SwiftUI

@main
struct _27_Adding_user_locations_to_a_map_Bucket_List_6_12App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
