//
//  Location.swift
//  127 Adding user locations to a map Bucket List 6 12
//
//  Created by Oleksandr Nesynov on 23.02.2023.
//

import Foundation

struct Location: Identifiable, Codable, Equatable {
    let id: UUID
    let name: String
    var description: String
    let latitude: Double
    let longitude: Double
}
