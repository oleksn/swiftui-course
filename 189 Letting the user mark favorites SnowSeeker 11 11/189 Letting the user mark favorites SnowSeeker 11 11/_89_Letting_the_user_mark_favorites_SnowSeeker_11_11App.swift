//
//  _89_Letting_the_user_mark_favorites_SnowSeeker_11_11App.swift
//  189 Letting the user mark favorites SnowSeeker 11 11
//
//  Created by Oleksandr Nesynov on 10.03.2023.
//

import SwiftUI

@main
struct _89_Letting_the_user_mark_favorites_SnowSeeker_11_11App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
