//
//  _56_Adding_a_context_menu_to_an_image_Hot_Prospects_17_18App.swift
//  156 Adding a context menu to an image Hot Prospects 17 18
//
//  Created by Oleksandr Nesynov on 03.03.2023.
//

import SwiftUI

@main
struct _56_Adding_a_context_menu_to_an_image_Hot_Prospects_17_18App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
