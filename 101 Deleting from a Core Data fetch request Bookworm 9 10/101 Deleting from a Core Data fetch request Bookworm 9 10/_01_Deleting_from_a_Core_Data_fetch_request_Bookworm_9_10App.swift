//
//  _01_Deleting_from_a_Core_Data_fetch_request_Bookworm_9_10App.swift
//  101 Deleting from a Core Data fetch request Bookworm 9 10
//
//  Created by Oleks on 16.02.2023.
//

import SwiftUI

@main
struct _01_Deleting_from_a_Core_Data_fetch_request_Bookworm_9_10App: App {
    let persistenceContainer = DataController()
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(
                    \.managedObjectContext,
                     persistenceContainer
                        .container
                        .viewContext
                )
        }
    }
}
