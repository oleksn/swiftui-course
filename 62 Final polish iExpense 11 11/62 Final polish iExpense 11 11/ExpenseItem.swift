//
//  ExpenseItem.swift
//  62 Final polish iExpense 11 11
//
//  Created by Oleks on 03.02.2023
//

import Foundation

struct ExpenseItem: Identifiable, Codable {
    var id = UUID()
    let name: String
    let type: String
    let amount: Double
}
