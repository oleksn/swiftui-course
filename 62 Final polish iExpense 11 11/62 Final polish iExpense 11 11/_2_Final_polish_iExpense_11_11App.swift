//
//  _2_Final_polish_iExpense_11_11App.swift
//  62 Final polish iExpense 11 11
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _2_Final_polish_iExpense_11_11App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
