//
//  _10_How_property_wrappers_become_structs_Instafilter_1_12App.swift
//  110 How property wrappers become structs Instafilter 1 12
//
//  Created by Oleks on 17.02.2023.
//

import SwiftUI

@main
struct _10_How_property_wrappers_become_structs_Instafilter_1_12App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
