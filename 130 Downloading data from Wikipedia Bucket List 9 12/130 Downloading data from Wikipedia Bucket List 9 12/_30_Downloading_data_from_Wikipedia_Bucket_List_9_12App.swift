//
//  _30_Downloading_data_from_Wikipedia_Bucket_List_9_12App.swift
//  130 Downloading data from Wikipedia Bucket List 9 12
//
//  Created by Oleksandr Nesynov on 28.02.2023.
//

import SwiftUI

@main
struct _30_Downloading_data_from_Wikipedia_Bucket_List_9_12App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
