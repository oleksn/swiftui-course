//
//  Result.swift
//  130 Downloading data from Wikipedia Bucket List 9 12
//
//  Created by Oleksandr Nesynov on 28.02.2023.
//

import Foundation

struct Result: Codable {
    let query: Query
}

struct Query: Codable {
    let pages: [Int: Page]
}

struct Page: Codable {
    let pageid: Int
    let title: String
    let terms: [String: [String]]?
}
