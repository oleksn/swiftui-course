//
//  _17_Building_our_basic_UI_Instafilter_8_12App.swift
//  117 Building our basic UI Instafilter 8 12
//
//  Created by Oleks on 17.02.2023.
//

import SwiftUI

@main
struct _17_Building_our_basic_UI_Instafilter_8_12App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
