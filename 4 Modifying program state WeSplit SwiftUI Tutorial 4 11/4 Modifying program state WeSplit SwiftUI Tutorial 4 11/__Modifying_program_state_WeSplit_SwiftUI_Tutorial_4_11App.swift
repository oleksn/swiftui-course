//
//  __Modifying_program_state_WeSplit_SwiftUI_Tutorial_4_11App.swift
//  4 Modifying program state WeSplit SwiftUI Tutorial 4 11
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct __Modifying_program_state_WeSplit_SwiftUI_Tutorial_4_11App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
