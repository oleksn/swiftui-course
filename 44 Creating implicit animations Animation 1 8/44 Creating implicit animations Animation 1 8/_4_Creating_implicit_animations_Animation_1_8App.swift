//
//  _4_Creating_implicit_animations_Animation_1_8App.swift
//  44 Creating implicit animations Animation 1 8
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _4_Creating_implicit_animations_Animation_1_8App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
