//
//  ContentView.swift
//  67 How to lay out views in a scrolling grid Moonshot 5 11
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

struct ContentView: View {
////    2)
//    let layout = [
//        GridItem(.fixed(80)),
//        GridItem(.fixed(80)),
//        GridItem(.fixed(80))
//    ]
////    1)
//    let layout = [
//        GridItem(.adaptive(minimum: 80))
//    ]
    
    let layout = [
        GridItem(.adaptive(minimum: 80, maximum: 120))
    ]
    
    
    var body: some View {
////        3)
//        ScrollView {
//            LazyVGrid(columns: layout) {
//                ForEach(0..<100) {
//                    Text("Item \($0)")
//                }
//            }
//        }
////    4)
        ScrollView(.horizontal) {
            LazyHGrid(rows: layout) {
                ForEach(0..<100) {
                    Text("Item \($0)")
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
