//
//  _7_How_to_lay_out_views_in_a_scrolling_grid_Moonshot_5_11App.swift
//  67 How to lay out views in a scrolling grid Moonshot 5 11
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _7_How_to_lay_out_views_in_a_scrolling_grid_Moonshot_5_11App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
