//
//  _1_Building_custom_transitions_using_ViewModifier_Animation_8_8App.swift
//  51 Building custom transitions using ViewModifier Animation 8 8
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _1_Building_custom_transitions_using_ViewModifier_Animation_8_8App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
