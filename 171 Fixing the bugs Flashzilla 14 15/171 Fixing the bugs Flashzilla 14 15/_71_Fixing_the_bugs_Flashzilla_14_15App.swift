//
//  _71_Fixing_the_bugs_Flashzilla_14_15App.swift
//  171 Fixing the bugs Flashzilla 14 15
//
//  Created by Oleksandr Nesynov on 07.03.2023.
//

import SwiftUI

@main
struct _71_Fixing_the_bugs_Flashzilla_14_15App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
