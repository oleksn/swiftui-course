//
//  _64_Designing_a_single_card_view_Flashzilla_7_15App.swift
//  164 Designing a single card view Flashzilla 7 15
//
//  Created by Oleksandr Nesynov on 03.03.2023.
//

import SwiftUI

@main
struct _64_Designing_a_single_card_view_Flashzilla_7_15App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
