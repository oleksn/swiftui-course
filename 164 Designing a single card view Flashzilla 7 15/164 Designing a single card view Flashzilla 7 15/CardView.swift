//
//  CardView.swift
//  164 Designing a single card view Flashzilla 7 15
//
//  Created by Oleksandr Nesynov on 03.03.2023.
//

import SwiftUI

import SwiftUI

struct CardView: View {
    let card: Card
    var removal: (() -> Void)? = nil

//    @State private var feedback = UINotificationFeedbackGenerator()
//
//    @Environment(\.accessibilityDifferentiateWithoutColor) var differentiateWithoutColor
//    @Environment(\.accessibilityVoiceOverEnabled) var voiceOverEnabled
//
//    @State private var offset = CGSize.zero
//
    @State private var isShowingAnswer = false

    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 25, style: .continuous)
                .fill(.white)
                .shadow(radius: 10)
            
            VStack {
                Text(card.prompt)
                    .font(.largeTitle)
                    .foregroundColor(.black)
                if isShowingAnswer {
                    Text(card.answer)
                        .font(.title)
                        .foregroundColor(.gray)
                }
            }
            .padding()
            .multilineTextAlignment(.center)
        }
        .frame(width: 450, height: 250)
        .onTapGesture {
            isShowingAnswer.toggle()
        }
    }
}

struct CardView_Previews: PreviewProvider {
    static var previews: some View {
        CardView(card: Card.example)
    }
}
