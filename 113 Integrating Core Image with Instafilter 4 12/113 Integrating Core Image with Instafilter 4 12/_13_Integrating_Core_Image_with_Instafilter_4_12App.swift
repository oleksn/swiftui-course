//
//  _13_Integrating_Core_Image_with_Instafilter_4_12App.swift
//  113 Integrating Core Image with Instafilter 4 12
//
//  Created by Oleks on 17.02.2023.
//

import SwiftUI

@main
struct _13_Integrating_Core_Image_with_Instafilter_4_12App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
