//
//  _37_Fixing_Guess_the_Flag_Accessibility_4_6App.swift
//  137 Fixing Guess the Flag Accessibility 4 6
//
//  Created by Oleksandr Nesynov on 02.03.2023.
//

import SwiftUI

@main
struct _37_Fixing_Guess_the_Flag_Accessibility_4_6App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
