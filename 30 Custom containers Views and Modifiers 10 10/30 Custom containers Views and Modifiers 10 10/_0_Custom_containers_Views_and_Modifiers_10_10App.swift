//
//  _0_Custom_containers_Views_and_Modifiers_10_10App.swift
//  30 Custom containers Views and Modifiers 10 10
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _0_Custom_containers_Views_and_Modifiers_10_10App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
