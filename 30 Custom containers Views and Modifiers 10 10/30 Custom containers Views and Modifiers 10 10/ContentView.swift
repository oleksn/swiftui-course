//
//  ContentView.swift
//  30 Custom containers Views and Modifiers 10 10
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

struct GridStack<Content: View>: View {
    let rows: Int
    let colomns: Int
////  1)
//    let content: (Int, Int) -> Content
    @ViewBuilder let content: (Int, Int) -> Content
    
    var body: some View {
        VStack {
//            ForEach(0..<rows) { row in
            ForEach(0..<rows, id: \.self) { row in
                HStack {
                    ForEach(0..<colomns, id: \.self) { colomn in
                        content(row, colomn)
                    }
                }
            }
        }
    }
}

struct ContentView: View {
    var body: some View {
        GridStack(rows: 4, colomns: 4) { row, col in
////          1)
//            VStack {
//                Image(systemName: "\(row * 4 + col).circle")
//                Text("R\(row) C\(col)")
//            }
////          2)
            Image(systemName: "\(row * 4 + col).circle")
            Text("R\(row) C\(col)")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
