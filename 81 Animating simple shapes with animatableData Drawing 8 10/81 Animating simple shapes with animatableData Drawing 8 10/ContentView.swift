//
//  ContentView.swift
//  81 Animating simple shapes with animatableData Drawing 8 10
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

struct Trapezoid: Shape {
    var insetAmount: Double
    
    var animatableData: Double {
        get { insetAmount }
        set { insetAmount = newValue }
    }
    
    func path(in rect: CGRect) -> Path {
        var path = Path()
         
        path.move(to: CGPoint(x: 0, y: rect.maxY))
        path.addLine(to: CGPoint.init(x: insetAmount, y: rect.minY))
        
        path.addLine(to: CGPoint.init(x: rect.maxX - insetAmount, y: rect.minY))
        path.addLine(to: CGPoint.init(x: rect.maxX, y: rect.maxY))
        path.addLine(to: CGPoint.init(x: 0, y: rect.maxY))
        
        return path
    }
}

struct ContentView: View {
    @State var insetAmount = 50.0
    
    
    var body: some View {
        Trapezoid(insetAmount: insetAmount)
            .frame(width: 200, height: 100)
            .onTapGesture {
                withAnimation {
                    insetAmount = Double.random(in: 10...90)
                }
            }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
