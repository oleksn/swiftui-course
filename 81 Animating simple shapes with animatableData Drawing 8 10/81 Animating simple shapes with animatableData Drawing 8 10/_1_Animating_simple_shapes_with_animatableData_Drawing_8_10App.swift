//
//  _1_Animating_simple_shapes_with_animatableData_Drawing_8_10App.swift
//  81 Animating simple shapes with animatableData Drawing 8 10
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _1_Animating_simple_shapes_with_animatableData_Drawing_8_10App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
