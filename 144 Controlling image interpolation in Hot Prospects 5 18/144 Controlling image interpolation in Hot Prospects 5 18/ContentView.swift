//
//  ContentView.swift
//  144 Controlling image interpolation in Hot Prospects 5 18
//
//  Created by Oleksandr Nesynov on 03.03.2023.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Image("example")
            .interpolation(.none)
            .resizable()
            .scaledToFit()
            .frame(maxHeight: .infinity)
            .background(.black)
            .ignoresSafeArea()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
