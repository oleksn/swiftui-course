//
//  _44_Controlling_image_interpolation_in_Hot_Prospects_5_18App.swift
//  144 Controlling image interpolation in Hot Prospects 5 18
//
//  Created by Oleksandr Nesynov on 03.03.2023.
//

import SwiftUI

@main
struct _44_Controlling_image_interpolation_in_Hot_Prospects_5_18App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
