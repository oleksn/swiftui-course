//
//  ContentView.swift
//  68 Loading a specific kind of Codable data Moonshot 6 11
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

struct ContentView: View {
    let astronauts = Bundle.main.decode("astronauts.json")
    var body: some View {
        Text("\(astronauts.count)")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
