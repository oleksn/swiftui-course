//
//  _8_Loading_a_specific_kind_of_Codable_data_Moonshot_6_11App.swift
//  68 Loading a specific kind of Codable data Moonshot 6 11
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _8_Loading_a_specific_kind_of_Codable_data_Moonshot_6_11App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
