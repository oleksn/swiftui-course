//
//  _9_Showing_book_details_Bookworm_7_10App.swift
//  99 Showing book details Bookworm 7 10
//
//  Created by Oleks on 16.02.2023.
//

import SwiftUI

@main
struct _9_Showing_book_details_Bookworm_7_10App: App {
    let persistenceContainer = DataController()
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(
                    \.managedObjectContext,
                     persistenceContainer
                        .container
                        .viewContext
                )
        }
    }
}
