//
//  _5_Sending_and_receiving_Codable_data_with_URLSession_and_Cupcake_Corner_2_9App.swift
//  85 Sending and receiving Codable data with URLSession and Cupcake Corner 2 9
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _5_Sending_and_receiving_Codable_data_with_URLSession_and_Cupcake_Corner_2_9App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
