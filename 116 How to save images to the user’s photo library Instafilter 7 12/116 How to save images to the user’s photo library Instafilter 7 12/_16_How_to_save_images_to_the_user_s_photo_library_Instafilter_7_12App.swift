//
//  _16_How_to_save_images_to_the_user_s_photo_library_Instafilter_7_12App.swift
//  116 How to save images to the user’s photo library Instafilter 7 12
//
//  Created by Oleks on 17.02.2023.
//

import SwiftUI

@main
struct _16_How_to_save_images_to_the_user_s_photo_library_Instafilter_7_12App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
