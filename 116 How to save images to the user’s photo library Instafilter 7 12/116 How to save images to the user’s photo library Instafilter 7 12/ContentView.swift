//
//  ContentView.swift
//  116 How to save images to the user’s photo library Instafilter 7 12
//
//  Created by Oleks on 17.02.2023.
//

import SwiftUI

struct ContentView: View {
    @State var image: Image?
    @State var inputImage: UIImage?
    @State var showingImagePicker = false
    
    var body: some View {
        VStack {
            image?
                .resizable()
                .scaledToFit()
            
            Button("Select Image") {
                showingImagePicker = true
            }
            
            Button("Save Image") {
                guard let inputImage = inputImage else { return }
                
                let imageSaver = ImageSaver()
                imageSaver.writToPhotoAlbum(image: inputImage)
            }
        }
        .sheet(isPresented: $showingImagePicker) {
            ImagePicker(image: $inputImage)
        }
        .onChange(of: inputImage) { _ in loadImage() }
    }
    
    func loadImage() {
        guard let inputImage = inputImage else { return }
        
        image = Image(uiImage: inputImage)
        
//        UIImageWriteToSavedPhotosAlbum(
//            inputImage, nil, nil, nil)
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

