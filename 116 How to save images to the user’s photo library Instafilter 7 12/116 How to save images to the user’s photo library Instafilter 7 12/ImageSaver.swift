//
//  ImageSaver.swift
//  116 How to save images to the user’s photo library Instafilter 7 12
//
//  Created by Oleks on 17.02.2023.
//

import UIKit

class ImageSaver: NSObject {
    func writToPhotoAlbum(image: UIImage) {
        UIImageWriteToSavedPhotosAlbum(
            image,
            self,
            #selector(saveCompleted),
            nil
        )
    }
    
    @objc func saveCompleted(
        _ image: UIImage,
        didFinishSavingWithError error: Error?,
        contextInfo: UnsafeRawPointer
    ) {
        print("Save finished!")
    }
}
