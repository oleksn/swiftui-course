//
//  ContentView.swift
//  115 Using coordinators to manage view controllers Instafilter 6 12
//
//  Created by Oleks on 17.02.2023.
//

import SwiftUI

struct ContentView: View {
    @State var image: Image?
    @State var inputImage: UIImage?
    @State var showingImagePicker = false
    
    var body: some View {
        VStack {
            image?
                .resizable()
                .scaledToFit()
            
            Button("Select Image") {
                showingImagePicker = true
            }
        }
        .sheet(isPresented: $showingImagePicker) {
            ImagePicker(image: $inputImage)
        }
        .onChange(of: inputImage) { _ in loadImage() }
    }
    
    func loadImage() {
        guard let inputImage = inputImage else { return }
        
        image = Image(uiImage: inputImage)
        
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
