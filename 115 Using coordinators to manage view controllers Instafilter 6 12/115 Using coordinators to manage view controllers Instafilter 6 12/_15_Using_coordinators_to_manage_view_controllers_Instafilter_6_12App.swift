//
//  _15_Using_coordinators_to_manage_view_controllers_Instafilter_6_12App.swift
//  115 Using coordinators to manage view controllers Instafilter 6 12
//
//  Created by Oleks on 17.02.2023.
//

import SwiftUI

@main
struct _15_Using_coordinators_to_manage_view_controllers_Instafilter_6_12App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
