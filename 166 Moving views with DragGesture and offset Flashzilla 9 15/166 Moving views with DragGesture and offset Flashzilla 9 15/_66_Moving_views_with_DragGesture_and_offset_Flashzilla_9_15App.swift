//
//  _66_Moving_views_with_DragGesture_and_offset_Flashzilla_9_15App.swift
//  166 Moving views with DragGesture and offset Flashzilla 9 15
//
//  Created by Oleksandr Nesynov on 03.03.2023.
//

import SwiftUI

@main
struct _66_Moving_views_with_DragGesture_and_offset_Flashzilla_9_15App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
