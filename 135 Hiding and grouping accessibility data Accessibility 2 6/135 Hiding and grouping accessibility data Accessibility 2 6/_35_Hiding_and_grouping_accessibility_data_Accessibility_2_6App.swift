//
//  _35_Hiding_and_grouping_accessibility_data_Accessibility_2_6App.swift
//  135 Hiding and grouping accessibility data Accessibility 2 6
//
//  Created by Oleksandr Nesynov on 02.03.2023.
//

import SwiftUI

@main
struct _35_Hiding_and_grouping_accessibility_data_Accessibility_2_6App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
