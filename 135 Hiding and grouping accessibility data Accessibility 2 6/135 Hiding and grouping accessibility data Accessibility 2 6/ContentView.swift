//
//  ContentView.swift
//  135 Hiding and grouping accessibility data Accessibility 2 6
//
//  Created by Oleksandr Nesynov on 02.03.2023.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
//        Image(
//            decorative: "character"
//        )
//        .accessibilityHidden(true)
        
        VStack {
            Text("Your score is")
            Text("1000")
                .font(.title)
        }
//        .accessibilityElement(
////            children: .combine
//            children: .ignore
//        )
        
        .accessibilityLabel("Your score is 1000")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
