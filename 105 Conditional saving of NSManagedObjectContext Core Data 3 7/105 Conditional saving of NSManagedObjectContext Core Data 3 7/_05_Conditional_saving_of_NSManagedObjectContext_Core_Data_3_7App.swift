//
//  _05_Conditional_saving_of_NSManagedObjectContext_Core_Data_3_7App.swift
//  105 Conditional saving of NSManagedObjectContext Core Data 3 7
//
//  Created by Oleks on 16.02.2023.
//

import SwiftUI

@main
struct _05_Conditional_saving_of_NSManagedObjectContext_Core_Data_3_7App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
