//
//  __Creating_a_form_WeSplit_SwiftUI_Tutorial_2_11App.swift
//  2 Creating a form WeSplit SwiftUI Tutorial 2 11
//
//  Created by Oleks on 27.01.2023.
//

import SwiftUI

@main
struct __Creating_a_form_WeSplit_SwiftUI_Tutorial_2_11App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
