//
//  ContentView.swift
//  15 Buttons and images Guess the Flag 4 9
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

struct ContentView: View {
    var body: some View {
////        1)
//        VStack {
//            Button("Delete selection", role: ButtonRole.destructive, action: executeDelete)
//        }
        
////        2)
//        VStack {
//            Button("Button 1") { }
//                .buttonStyle(.bordered)
//            Button("Button 2", role: .destructive) { }
//                .buttonStyle(.bordered)
//            Button("Button 3") { }
//                .buttonStyle(.borderedProminent)
//            Button("Button 4", role: .destructive) { }
//                .buttonStyle(.borderedProminent)
//            Button("Button 5") { }
//                .buttonStyle(.borderedProminent)
//                .tint(.mint)
        
////        3)
//        Button {
//            print("Button was patted")
//        } label: {
//            Text("tap me!")
//                .padding()
//                .foregroundColor(.white)
//                .background(.red)
//        }
        
        Button {
            print("Edit button was tapped!")
        } label: {
//            Image(systemName: "pencil")
            Label("Edit", systemImage: "pencil")
        }
        .symbolRenderingMode(.monochrome)
    }
    
    func executeDelete() {
        print("Now delete")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
