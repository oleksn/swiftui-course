//
//  _5_Buttons_and_images_Guess_the_Flag_4_9App.swift
//  15 Buttons and images Guess the Flag 4 9
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _5_Buttons_and_images_Guess_the_Flag_4_9App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
