//
//  _49_Building_our_tab_bar_Hot_Prospects_10_18App.swift
//  149 Building our tab bar Hot Prospects 10 18
//
//  Created by Oleksandr Nesynov on 03.03.2023.
//

import SwiftUI

@main
struct _49_Building_our_tab_bar_Hot_Prospects_10_18App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
