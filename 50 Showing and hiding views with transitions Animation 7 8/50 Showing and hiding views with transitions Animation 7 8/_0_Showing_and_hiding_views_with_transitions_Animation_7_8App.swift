//
//  _0_Showing_and_hiding_views_with_transitions_Animation_7_8App.swift
//  50 Showing and hiding views with transitions Animation 7 8
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _0_Showing_and_hiding_views_with_transitions_Animation_7_8App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
