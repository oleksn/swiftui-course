//
//  ContentView.swift
//  50 Showing and hiding views with transitions Animation 7 8
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

struct ContentView: View {
    @State var isShowingRed = false
    
    var body: some View {
        VStack {
            Button("Tap Me") {
//// 1)
//                isShowingRed.toggle()
                withAnimation {
                    isShowingRed.toggle()
                }
            }
            if isShowingRed {
                Rectangle()
                    .fill(.red)
                    .frame(width: 200, height: 200)
//// 2)
//                    .transition(.scale)
                    .transition(.asymmetric(
                        insertion: .scale, removal: .opacity
                    ))
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
