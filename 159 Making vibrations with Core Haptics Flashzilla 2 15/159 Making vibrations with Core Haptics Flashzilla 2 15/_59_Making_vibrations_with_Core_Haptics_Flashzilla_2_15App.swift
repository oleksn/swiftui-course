//
//  _59_Making_vibrations_with_Core_Haptics_Flashzilla_2_15App.swift
//  159 Making vibrations with Core Haptics Flashzilla 2 15
//
//  Created by Oleksandr Nesynov on 03.03.2023.
//

import SwiftUI

@main
struct _59_Making_vibrations_with_Core_Haptics_Flashzilla_2_15App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
