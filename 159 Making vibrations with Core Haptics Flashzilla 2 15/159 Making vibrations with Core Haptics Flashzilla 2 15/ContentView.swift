//
//  ContentView.swift
//  159 Making vibrations with Core Haptics Flashzilla 2 15
//
//  Created by Oleksandr Nesynov on 03.03.2023.
//

import CoreHaptics
import SwiftUI

struct ContentView: View {
    @State var engine: CHHapticEngine?
    
    var body: some View {
        VStack {
            Image(systemName: "globe")
                .imageScale(.large)
                .foregroundColor(.accentColor)
            Text("Hello, world!")
                .onAppear(perform: prepareHaptics)
                .onTapGesture(perform: complexSuccess)
        }
        .padding()
//        .onTapGesture(perform: simpleSuccess)
    }
    
    func simpleSuccess() {
        let generator = UINotificationFeedbackGenerator()
//        generator.notificationOccurred(.success)
        generator.notificationOccurred(.error)
    }
    
    func prepareHaptics() {
        guard CHHapticEngine
            .capabilitiesForHardware()
            .supportsHaptics else { return }
        
        do {
            engine = try CHHapticEngine()
            try engine?.start()
        } catch {
            print("$$$ error \(error.localizedDescription)")
        }
    }
    
    func complexSuccess() {
        guard CHHapticEngine
            .capabilitiesForHardware()
            .supportsHaptics else { return }
        
        var events = [CHHapticEvent]()
        
        for i in stride(from: 0, through: 1, by: 0.1) {
            let intensity = CHHapticEventParameter(
                parameterID: .hapticIntensity,
                value: Float(i)
            )
            
            let sharpness = CHHapticEventParameter(
                parameterID: .hapticSharpness,
                value: Float(i)
            )
            
            let event = CHHapticEvent(
                eventType: .hapticTransient,
                parameters: [intensity, sharpness ],
                relativeTime: i
            )
            
            events.append(event)
        }
        
        for i in stride(from: 0, through: 1, by: 0.1) {
            let intensity = CHHapticEventParameter(
                parameterID: .hapticIntensity,
                value: Float(1 - i)
            )
            
            let sharpness = CHHapticEventParameter(
                parameterID: .hapticSharpness,
                value: Float(1 - i)
            )
            
            let event = CHHapticEvent(
                eventType: .hapticTransient,
                parameters: [intensity, sharpness ],
                relativeTime: 1 + i
            )
            
            events.append(event)
        }
        
        do {
            let pattern = try CHHapticPattern(
                events: events,
                parameters: []
            )
            let player = try engine?.makePlayer(with: pattern)
            try player?.start(atTime: 0)
        } catch {
            print("error \(error.localizedDescription)")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
