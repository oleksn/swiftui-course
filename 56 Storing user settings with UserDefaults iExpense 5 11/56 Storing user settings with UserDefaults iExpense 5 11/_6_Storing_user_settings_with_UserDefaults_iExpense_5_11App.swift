//
//  _6_Storing_user_settings_with_UserDefaults_iExpense_5_11App.swift
//  56 Storing user settings with UserDefaults iExpense 5 11
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _6_Storing_user_settings_with_UserDefaults_iExpense_5_11App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
