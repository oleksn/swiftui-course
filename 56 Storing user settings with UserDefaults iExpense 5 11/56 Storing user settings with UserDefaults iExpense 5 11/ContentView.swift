//
//  ContentView.swift
//  56 Storing user settings with UserDefaults iExpense 5 11
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

struct ContentView: View {
    // @State private var tapCount = UserDefaults.standard.integer(forKey: "Tap")
    @AppStorage("tapCount") private var tapCount = 0
    
    
    var body: some View {
        Button("Tap count: \(tapCount)") {
            tapCount += 1
            // UserDefaults.standard.set(tapCount, forKey: "Tap")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
