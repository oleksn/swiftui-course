//
//  ContentView.swift
//  141 Creating tabs with TabView and tabItem Hot Prospects 2 18
//
//  Created by Oleksandr Nesynov on 02.03.2023.
//

import SwiftUI

struct ContentView: View {
//    static let tag = "ContentView"
    @State var selectedTab = "One"
    
    var body: some View {
        TabView(selection: $selectedTab) {
            Text("Tab 1")
                .onTapGesture {
                    selectedTab = "Two"
                }
                .tabItem {
                    Label("One", systemImage: "star")
                }
            
            Text("Tab 2")
                .tabItem {
                    Label("Two", systemImage: "circle")
                }
                .tag("Two")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
