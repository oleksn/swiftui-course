//
//  _41_Creating_tabs_with_TabView_and_tabItem_Hot_Prospects_2_18App.swift
//  141 Creating tabs with TabView and tabItem Hot Prospects 2 18
//
//  Created by Oleksandr Nesynov on 02.03.2023.
//

import SwiftUI

@main
struct _41_Creating_tabs_with_TabView_and_tabItem_Hot_Prospects_2_18App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
