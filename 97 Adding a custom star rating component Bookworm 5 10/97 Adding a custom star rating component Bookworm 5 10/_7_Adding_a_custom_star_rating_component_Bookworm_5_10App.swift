//
//  _7_Adding_a_custom_star_rating_component_Bookworm_5_10App.swift
//  97 Adding a custom star rating component Bookworm 5 10
//
//  Created by Oleks on 16.02.2023.
//

import SwiftUI

@main
struct _7_Adding_a_custom_star_rating_component_Bookworm_5_10App: App {
    let persistenceContainer = DataController()
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceContainer.container.viewContext)
        }
    }
}
