//
//  _9_Enabling_highperformance_Metal_rendering_with_drawingGroup_Drawing_6_10App.swift
//  79 Enabling highperformance Metal rendering with drawingGroup Drawing 6 10
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _9_Enabling_highperformance_Metal_rendering_with_drawingGroup_Drawing_6_10App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
