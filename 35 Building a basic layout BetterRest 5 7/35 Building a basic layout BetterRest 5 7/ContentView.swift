//
//  ContentView.swift
//  35 Building a basic layout BetterRest 5 7
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

struct ContentView: View {
    @State var wakeUp = Date.now
    @State var sleepAmount = 8.0
    @State var coffeeAmount = 1
    
    var body: some View {
        NavigationView {
            VStack {
                Text("When do you want to wake up?")
                    .font(.headline)
                
                DatePicker(
                    "Please enter a time?",
                    selection: $wakeUp,
                    displayedComponents: .hourAndMinute
                )
                .labelsHidden()
                
                Text("Desired amount of sleep")
                    .font(.headline)
                
                Stepper(
                    "\(sleepAmount.formatted()) hours",
                    value: $sleepAmount,
                    in: 4...12,
                    step: 0.25
                )
                
                Text("Daily coffee intake")
                    .font(.headline)
                
                Stepper(
                    coffeeAmount == 1
                    ?
                    "1 cup" :
                    "\(coffeeAmount) cups",
                    value: $coffeeAmount,
                    in: 1...20
                )
            }
            .navigationTitle("BetterRest")
            .toolbar {
                Button(
                    "Calculate",
                    action: calculationBedtime
                )
            }
        }
    }
    
    func calculationBedtime() {
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
