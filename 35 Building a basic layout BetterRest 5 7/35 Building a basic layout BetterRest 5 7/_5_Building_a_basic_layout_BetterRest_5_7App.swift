//
//  _5_Building_a_basic_layout_BetterRest_5_7App.swift
//  35 Building a basic layout BetterRest 5 7
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _5_Building_a_basic_layout_BetterRest_5_7App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
