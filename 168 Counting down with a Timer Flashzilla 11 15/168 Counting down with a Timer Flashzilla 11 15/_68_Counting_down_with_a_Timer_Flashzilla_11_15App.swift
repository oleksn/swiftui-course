//
//  _68_Counting_down_with_a_Timer_Flashzilla_11_15App.swift
//  168 Counting down with a Timer Flashzilla 11 15
//
//  Created by Oleksandr Nesynov on 03.03.2023.
//

import SwiftUI

@main
struct _68_Counting_down_with_a_Timer_Flashzilla_11_15App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
