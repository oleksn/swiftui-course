//
//  _14_Wrapping_a_UIViewController_in_a_view_Instafilter_5_12App.swift
//  114 Wrapping a UIViewController in a view Instafilter 5 12
//
//  Created by Oleks on 17.02.2023.
//

import SwiftUI

@main
struct _14_Wrapping_a_UIViewController_in_a_view_Instafilter_5_12App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
