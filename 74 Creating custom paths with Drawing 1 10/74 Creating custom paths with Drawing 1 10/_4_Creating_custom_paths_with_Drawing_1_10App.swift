//
//  _4_Creating_custom_paths_with_Drawing_1_10App.swift
//  74 Creating custom paths with Drawing 1 10
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _4_Creating_custom_paths_with_Drawing_1_10App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
