//
//  ContentView.swift
//  74 Creating custom paths with Drawing 1 10
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Path { path in
            path.move(to: CGPoint.init(x: 200, y: 100))
            path.addLine(to: CGPoint.init(x: 100, y: 300))
            path.addLine(to: CGPoint.init(x: 300, y: 300))
            path.addLine(to: CGPoint.init(x: 200, y: 100))
            path.closeSubpath()
        }
//        .fill(.blue)
//        .stroke(.blue, lineWidth: 10)
        .stroke(.blue, style: StrokeStyle(
            lineWidth: 10,
            lineCap: CGLineCap.round,
            lineJoin: CGLineJoin.round
        ))
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
