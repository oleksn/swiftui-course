//
//  ContentView.swift
//  177 Understanding frames and coordinates Layout and Geometry 5 6
//
//  Created by Oleksandr Nesynov on 10.03.2023.
//

import SwiftUI

struct OuterView: View {
    var body: some View {
        VStack {
            Text("Top")
            InnerView()
                .background(.green)
            Text("Bottom")
        }
    }
}

struct InnerView: View {
    var body: some View {
        HStack {
            Text("Left")
            
            GeometryReader { geo in
                Text("Center")
                    .background(.blue)
                    .onTapGesture {
                        print("Global center: \(geo.frame(in: .global).minX) x \(geo.frame(in: .global).minY)")
                        
                        print("Local center: \(geo.frame(in: .local).minX) x \(geo.frame(in: .local).minY)")
                        
                        print("Custom center: \(geo.frame(in: .named("Custom")).minX) x \(geo.frame(in: .named("Custom")).minY)")
                    }
                    .background(.orange)
            }
            
            Text("Right")
        }
    }
}

////         1)
//struct ContentView: View {
//    var body: some View {
//        VStack {
//            GeometryReader { geo in
//                Text("Hello, world!")
//                    .frame(width: geo.size.width * 0.9)
//                    .background(.red)
//            }
//            .background(.green)
//
//            Text("More text")
//            Text("More text")
//            Text("More text")
//            Text("More text")
//            Text("More text")
//
//            Text("More text")
//                .background(.blue)
//        }
//    }
//}

////         2)
struct ContentView: View {
    var body: some View {
        OuterView()
            .background(.red)
            .coordinateSpace(name: "Custom")
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
