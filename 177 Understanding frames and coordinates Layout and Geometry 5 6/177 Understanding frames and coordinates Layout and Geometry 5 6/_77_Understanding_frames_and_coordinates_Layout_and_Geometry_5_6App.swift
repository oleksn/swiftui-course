//
//  _77_Understanding_frames_and_coordinates_Layout_and_Geometry_5_6App.swift
//  177 Understanding frames and coordinates Layout and Geometry 5 6
//
//  Created by Oleksandr Nesynov on 10.03.2023.
//

import SwiftUI

@main
struct _77_Understanding_frames_and_coordinates_Layout_and_Geometry_5_6App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
