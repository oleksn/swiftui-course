//
//  _3_Why_modifier_order_matters_Views_and_Modifiers_3_10App.swift
//  23 Why modifier order matters Views and Modifiers 3 10
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _3_Why_modifier_order_matters_Views_and_Modifiers_3_10App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
