//
//  Expenses.swift
//  61 Making changes permanent with UserDefaults iExpense 10 11
//
//  Created by Oleks on 03.02.2023
//

import Foundation

class Expenses: ObservableObject {
    @Published var items = [ExpenseItem]() {
        didSet {
            if let encoded = try? JSONEncoder().encode(items) {
                UserDefaults.standard.set(encoded, forKey: "Items")
            }
        }
    }

    init() {
        if let savedItems = UserDefaults.standard.data(
            forKey: "Items"
        ) {
            if let decodedItems = try? JSONDecoder().decode(
                [ExpenseItem].self, from: savedItems
            ) {
                items = decodedItems
                return
            }
        }

        items = []
    }
}
