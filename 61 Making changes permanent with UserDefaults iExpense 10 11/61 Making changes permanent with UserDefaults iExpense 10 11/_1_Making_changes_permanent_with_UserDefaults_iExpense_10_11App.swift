//
//  _1_Making_changes_permanent_with_UserDefaults_iExpense_10_11App.swift
//  61 Making changes permanent with UserDefaults iExpense 10 11
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _1_Making_changes_permanent_with_UserDefaults_iExpense_10_11App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
