//
//  ExpenseItem.swift
//  61 Making changes permanent with UserDefaults iExpense 10 11
//
//  Created by Oleks on 03.02.2023
//

import Foundation

struct ExpenseItem: Identifiable, Codable {
    var id = UUID()
    let name: String
    let type: String
    let amount: Double
}
