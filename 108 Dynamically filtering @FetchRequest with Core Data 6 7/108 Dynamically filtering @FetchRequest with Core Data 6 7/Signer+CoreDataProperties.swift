//
//  Signer+CoreDataProperties.swift
//  108 Dynamically filtering @FetchRequest with Core Data 6 7
//
//  Created by Oleks on 17.02.2023.
//
//

import Foundation
import CoreData


extension Signer {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Signer> {
        return NSFetchRequest<Signer>(entityName: "Signer")
    }

    @NSManaged public var firstName: String?
    @NSManaged public var lastName: String?
    
    var wrappedFirstName: String {
        firstName ?? "Unknown"
    }
    
    var wrappedLastName: String {
        lastName ?? "Unknown"
    }

}

extension Signer : Identifiable {

}
