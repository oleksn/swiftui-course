//
//  FilteredList.swift
//  108 Dynamically filtering @FetchRequest with Core Data 6 7
//
//  Created by Oleks on 17.02.2023.
//

import CoreData
import SwiftUI

struct FilteredList<T: NSManagedObject, Content: View>: View {
    // @State private var score = 0
    @FetchRequest var fetchRequest: FetchedResults<T>
    let content: (T) -> Content
    
    var body: some View {
//        List(fetchRequest, id: \.self) { signer in
//            Text("\(signer.wrappedFirstName) \(signer.wrappedLastName)")
//        }
        
        List(fetchRequest, id: \.self) { item in
            self.content(item)
        }
    }
    
//    init(filter: String)  {
//        _fetchRequest = FetchRequest<T>(
//            sortDescriptors: [],
//            predicate: NSPredicate(
//                format: "lastName BEGINSWITH %@", filter
//            )
//        )
//    }
    
    init(filterKey: String, filterValue: String, @ViewBuilder content: @escaping (T) -> Content)  {
        _fetchRequest = FetchRequest<T>(
            sortDescriptors: [],
            predicate: NSPredicate(
                format: "%K BEGINSWITH %@", filterKey, filterValue
            )
        )
        
        self.content = content
    }
}

//struct FilteredList_Previews: PreviewProvider {
//    static var previews: some View {
//        FilteredList()
//    }
//}
