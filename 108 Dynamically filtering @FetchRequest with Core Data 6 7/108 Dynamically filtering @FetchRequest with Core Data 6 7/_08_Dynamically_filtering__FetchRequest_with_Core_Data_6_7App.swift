//
//  _08_Dynamically_filtering__FetchRequest_with_Core_Data_6_7App.swift
//  108 Dynamically filtering @FetchRequest with Core Data 6 7
//
//  Created by Oleks on 16.02.2023.
//

import SwiftUI

@main
struct _08_Dynamically_filtering__FetchRequest_with_Core_Data_6_7App: App {
    let persistenceContainer = DataController()
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(
                    \.managedObjectContext,
                     persistenceContainer
                        .container
                        .viewContext
                )
        }
    }
}
