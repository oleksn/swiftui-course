//
//  ContentView.swift
//  108 Dynamically filtering @FetchRequest with Core Data 6 7
//
//  Created by Oleks on 16.02.2023.
//

import CoreData
import SwiftUI

struct ContentView: View {
    @Environment(\.managedObjectContext) var moc
    @State var lastNameFilter = "A"
    
    var body: some View {
        VStack {
            // list of match signers
//            FilteredList(filter: lastNameFilter)
            
            FilteredList(
                filterKey: "lastName",
                filterValue: lastNameFilter
            ) { (signer: Signer) in
                Text("\(signer.wrappedFirstName) \(signer.wrappedLastName)")
            }

            Button("Add Examples") {
                let taylor = Signer(context: moc)
                taylor.firstName = "Taylor"
                taylor.lastName = "Swift"
                
                let ed = Signer(context: moc)
                ed.firstName = "Ed"
                ed.lastName = "Sheeran"
                
                let adele = Signer(context: moc)
                adele.firstName = "Adele"
                adele.lastName = "Adkins"
                
                try? moc.save()
            }
            
            Button("Show A") {
                lastNameFilter = "A"
            }
            
            Button("Show S") {
                lastNameFilter = "S"
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
