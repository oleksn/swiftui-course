//
//  _63_Supporting_specific_accessibility_needs_with_Flashzilla_6_15App.swift
//  163 Supporting specific accessibility needs with Flashzilla 6 15
//
//  Created by Oleksandr Nesynov on 03.03.2023.
//

import SwiftUI

@main
struct _63_Supporting_specific_accessibility_needs_with_Flashzilla_6_15App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
