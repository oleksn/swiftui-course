//
//  _8_Taking_basic_order_details_Cupcake_Corner_5_9App.swift
//  88 Taking basic order details Cupcake Corner 5 9
//
//  Created by Oleks on 07.02.2023.
//

import SwiftUI

@main
struct _8_Taking_basic_order_details_Cupcake_Corner_5_9App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
