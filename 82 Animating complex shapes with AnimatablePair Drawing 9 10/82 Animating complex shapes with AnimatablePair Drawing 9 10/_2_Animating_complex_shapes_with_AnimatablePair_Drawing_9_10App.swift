//
//  _2_Animating_complex_shapes_with_AnimatablePair_Drawing_9_10App.swift
//  82 Animating complex shapes with AnimatablePair Drawing 9 10
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _2_Animating_complex_shapes_with_AnimatablePair_Drawing_9_10App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
