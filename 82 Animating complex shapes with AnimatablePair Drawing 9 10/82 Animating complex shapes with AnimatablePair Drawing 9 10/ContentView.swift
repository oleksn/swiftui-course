//
//  ContentView.swift
//  82 Animating complex shapes with AnimatablePair Drawing 9 10
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

struct Checkerboard: Shape {
    var rows: Int
    var colomns: Int
    
    var animatableData: AnimatablePair<Double, Double> {
        get {
            AnimatablePair(Double(rows), Double(colomns))
        }
        
        set {
            rows = Int(newValue.first)
            colomns = Int(newValue.second)
        }
    }
    
    func path(in rect: CGRect) -> Path {
        var path = Path()
        
        let rowSize = rect.height / Double(rows)
        let colomnsSize = rect.width / Double(colomns)
        
        for row in 0..<rows {
            for column in 0..<colomns {
                if (row + column).isMultiple(of: 2) {
                    let startX = colomnsSize * Double(column)
                    let startY = rowSize * Double(row)
                    
                    let rect = CGRect(
                        x: startX, y: startY,
                        width: colomnsSize, height: rowSize
                    )
                    path.addRect(rect)
                }
            }
        }
        
        return path
    }
}



struct ContentView: View {
    @State var rows = 4
    @State var columns = 4
    
    var body: some View {
        print("$$$ body")
        return Checkerboard(rows: rows, colomns: columns)
            .onTapGesture {
                withAnimation(.linear(duration: 3)) {
                    rows = 8
                    columns = 16
                }
            }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
