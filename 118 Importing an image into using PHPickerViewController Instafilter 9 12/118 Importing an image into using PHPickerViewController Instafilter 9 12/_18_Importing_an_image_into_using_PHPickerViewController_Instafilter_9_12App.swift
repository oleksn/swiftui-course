//
//  _18_Importing_an_image_into_using_PHPickerViewController_Instafilter_9_12App.swift
//  118 Importing an image into using PHPickerViewController Instafilter 9 12
//
//  Created by Oleks on 17.02.2023.
//

import SwiftUI

@main
struct _18_Importing_an_image_into_using_PHPickerViewController_Instafilter_9_12App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
