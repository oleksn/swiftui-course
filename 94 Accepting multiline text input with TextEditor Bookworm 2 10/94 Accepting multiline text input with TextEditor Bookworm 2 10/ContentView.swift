//
//  ContentView.swift
//  94 Accepting multiline text input with TextEditor Bookworm 2 10
//
//  Created by Oleks on 16.02.2023.
//

import SwiftUI

struct ContentView: View {
    @AppStorage("notes") var notes = ""
    
    
    var body: some View {
        NavigationView {
            TextEditor(text: $notes)
                .navigationTitle("Notes")
                .padding()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
