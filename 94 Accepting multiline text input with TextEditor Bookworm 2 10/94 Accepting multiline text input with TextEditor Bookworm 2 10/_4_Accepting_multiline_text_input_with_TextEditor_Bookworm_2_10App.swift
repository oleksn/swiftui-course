//
//  _4_Accepting_multiline_text_input_with_TextEditor_Bookworm_2_10App.swift
//  94 Accepting multiline text input with TextEditor Bookworm 2 10
//
//  Created by Oleks on 16.02.2023.
//

import SwiftUI

@main
struct _4_Accepting_multiline_text_input_with_TextEditor_Bookworm_2_10App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
