//
//  ContentView.swift
//  49 Animating gestures Animation 6 8
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

struct ContentView: View {
    let letters = Array("Hello, SwiftUI")
    @State var enabled = false
    @State var dragAmount = CGSize.zero
    
    
    var body: some View {
/////////////// Animation Part 1
//        LinearGradient(gradient: Gradient(colors: [.yellow, .red]),
//                       startPoint: UnitPoint.topLeading,
//                       endPoint: UnitPoint.bottomTrailing
//        )
//        .frame(width: 300, height: 200)
//        .clipShape(RoundedRectangle(cornerRadius: 10))
//        .offset(dragAmount)
//        .gesture(
//            DragGesture()
//                .onChanged { dragAmount = $0.translation }
//                .onEnded { _ in
//                    withAnimation {
//                        dragAmount = .zero
//                    }
//                }
//        )
//////    1)
////         .animation(.spring(), value: dragAmount) // -> implicit animation

///////////// Animation Part 2
        HStack(spacing: 0) {
            ForEach(0..<letters.count) { num in
                Text(String(letters[num]))
                    .padding(5)
                    .font(.title)
                    .background(enabled ? .blue : .red)
                    .offset(dragAmount)
                    .animation(
                        .default.delay(Double(num) / 20),
                        value: dragAmount
                    )
            }
        }
        .gesture(
            DragGesture()
                .onChanged { dragAmount = $0.translation }
                .onEnded { _ in
                    withAnimation {
                        dragAmount = .zero
                        enabled.toggle()
                    }
                }
        )
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
