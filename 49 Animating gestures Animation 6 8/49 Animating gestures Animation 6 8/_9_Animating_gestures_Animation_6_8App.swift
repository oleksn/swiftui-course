//
//  _9_Animating_gestures_Animation_6_8App.swift
//  49 Animating gestures Animation 6 8
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _9_Animating_gestures_Animation_6_8App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
