//
//  _9_Styling_our_flags_Guess_the_Flag_8_9App.swift
//  19 Styling our flags Guess the Flag 8 9
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _9_Styling_our_flags_Guess_the_Flag_8_9App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
