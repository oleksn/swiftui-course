//
//  _74_Alignment_and_alignment_guides_Layout_and_Geometry_2_6App.swift
//  174 Alignment and alignment guides Layout and Geometry 2 6
//
//  Created by Oleksandr Nesynov on 09.03.2023.
//

import SwiftUI

@main
struct _74_Alignment_and_alignment_guides_Layout_and_Geometry_2_6App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
