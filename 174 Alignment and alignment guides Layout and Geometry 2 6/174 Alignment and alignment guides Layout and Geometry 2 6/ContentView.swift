//
//  ContentView.swift
//  174 Alignment and alignment guides Layout and Geometry 2 6
//
//  Created by Oleksandr Nesynov on 09.03.2023.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
//             //1)
//        Text("Live long and prosper!")
//            .frame(
//                width: 300,
//                height: 300,
//                alignment: .topLeading
//            )
//            .offset(x: 50, y: 50)
//            .background(.red)

////             //2)
////        HStack(alignment: .bottom) {
//        HStack(alignment: VerticalAlignment.lastTextBaseline) {
//            Text("Live")
//                .font(.caption)
//            Text("long")
//            Text("and")
//                .font(.title)
//            Text("prosper!")
//                .font(.largeTitle)

////             //3)
//        VStack(alignment: .leading) {
//            Text("Hello, world!")
//                .alignmentGuide(.leading) { d in
//                    d[.trailing]
//                }
////                .offset(x: -100, y: 0)
//            Text("This is a longer line of text")
//        }
//        .background(.red)
//        .frame(width: 400, height: 400)
//        .background(.blue)
       
////             //4)
        VStack(alignment: .leading) {
            ForEach(0..<10) { position in
                Text("Number \(position)")
                    .alignmentGuide(.leading) { _ in
                        Double(position) * -10
                    }
            }
        }
        .background(.red)
        .frame(width: 400, height: 400)
        .background(.blue)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
