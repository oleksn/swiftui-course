//
//  ContentView.swift
//  140 Reading custom values from the environment with EnvironmentObject Hot Prospects 1 18
//
//  Created by Oleksandr Nesynov on 02.03.2023.
//

import SwiftUI

@MainActor class User: ObservableObject {
    @Published var name = "Taylor Swift"
}

struct EditView: View {
    @EnvironmentObject var user: User
    
    var body: some View {
        TextField(
            "Name",
            text: $user.name
        )
    }
}

struct DisplayView: View {
    @EnvironmentObject var user: User
    
    var body: some View {
        Text(user.name)
    }
}


struct ContentView: View {
//    @State
//    @ObservedObject
//    @EnvironmentObject
    
    @StateObject var user = User()
    
    
    
    var body: some View {
        VStack {
//            EditView()
//                .environmentObject(
//                    user
//                )
            
//            DisplayView()
//                .environmentObject(
//                    user
//                )
            EditView()
            DisplayView()
        }
        .environmentObject(user)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
