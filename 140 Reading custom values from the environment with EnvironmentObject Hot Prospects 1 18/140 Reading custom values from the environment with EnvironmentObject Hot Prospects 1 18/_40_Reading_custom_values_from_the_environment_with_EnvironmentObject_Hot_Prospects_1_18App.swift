//
//  _40_Reading_custom_values_from_the_environment_with_EnvironmentObject_Hot_Prospects_1_18App.swift
//  140 Reading custom values from the environment with EnvironmentObject Hot Prospects 1 18
//
//  Created by Oleksandr Nesynov on 02.03.2023.
//

import SwiftUI

@main
struct _40_Reading_custom_values_from_the_environment_with_EnvironmentObject_Hot_Prospects_1_18App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
