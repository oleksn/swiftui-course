//
//  _58_How_to_use_gestures_in_Flashzilla_1_15App.swift
//  158 How to use gestures in Flashzilla 1 15
//
//  Created by Oleksandr Nesynov on 03.03.2023.
//

import SwiftUI

@main
struct _58_How_to_use_gestures_in_Flashzilla_1_15App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
