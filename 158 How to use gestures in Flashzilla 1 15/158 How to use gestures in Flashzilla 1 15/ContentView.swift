 //
//  ContentView.swift
//  158 How to use gestures in Flashzilla 1 15
//
//  Created by Oleksandr Nesynov on 03.03.2023.
//

import SwiftUI

struct ContentView: View {
    @State var currentAmount = 0.0
    @State var finalAmount = 1.0
    @State var offset = CGSize.zero
    @State var isDragging = false
    
    var body: some View {
        let dragGesture = DragGesture()
            .onChanged { value in
                offset = value.translation
            }
            .onEnded { _ in
                offset = .zero
                isDragging = false
            }
        
        let pressGesture = LongPressGesture()
            .onEnded { value in
                withAnimation {
                    isDragging = true
                }
            }
        
        let combined = pressGesture.sequenced(before: dragGesture)
        
        Circle()
            .fill(.red)
            .frame(width: 64, height: 64)
            .scaleEffect(isDragging ? 1.5 : 1)
            .offset(offset)
            .gesture(combined)
        
//      // 1
//        VStack {
//            Image(systemName: "globe")
//                .imageScale(.large)
//                .foregroundColor(.accentColor)
//            Text("Hello, world!")
////                .onTapGesture(count: 2) {
////                    print("Hi")
////                }
////                .onLongPressGesture {
////                    print("Long!")
////                }
//                .onLongPressGesture(minimumDuration: 1) {
//                    print("Long!")
//                } onPressingChanged: { isProgress in
//                    print("In Progress: \(isProgress)")
//                }
//        }
//        .padding()
        
//        // 2
//        Text("Hello, world!")
//            .scaleEffect(currentAmount + finalAmount)
//            .gesture(
//                MagnificationGesture()
//                    .onChanged { amount in
//                        currentAmount = amount - 1
//                    }
//                    .onEnded { amount in
//                        finalAmount += currentAmount
//                        currentAmount = 0
//                    }
//            )
        
//        // 3
//        VStack {
//            Text("Hello, world!")
//                .onTapGesture {
//                    print("TAP!")
//                }
//        }
////        .onTapGesture {
////            print("VStack - TAP!")
////        }
////        .highPriorityGesture(
////            TapGesture()
////                .onEnded {
////                    print("VStack - TAP!")
////                }
////        )
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
