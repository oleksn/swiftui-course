//
//  _54_Adding_options_with_swipe_actions_Hot_Prospects_15_18App.swift
//  154 Adding options with swipe actions Hot Prospects 15 18
//
//  Created by Oleksandr Nesynov on 03.03.2023.
//

import SwiftUI

@main
struct _54_Adding_options_with_swipe_actions_Hot_Prospects_15_18App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
