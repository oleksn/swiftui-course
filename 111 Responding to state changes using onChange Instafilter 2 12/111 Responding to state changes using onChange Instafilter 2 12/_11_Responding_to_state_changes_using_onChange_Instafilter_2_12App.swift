//
//  _11_Responding_to_state_changes_using_onChange_Instafilter_2_12App.swift
//  111 Responding to state changes using onChange Instafilter 2 12
//
//  Created by Oleks on 17.02.2023.
//

import SwiftUI

@main
struct _11_Responding_to_state_changes_using_onChange_Instafilter_2_12App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
