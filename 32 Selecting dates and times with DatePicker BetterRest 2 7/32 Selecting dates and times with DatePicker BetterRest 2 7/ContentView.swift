//
//  ContentView.swift
//  32 Selecting dates and times with DatePicker BetterRest 2 7
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

struct ContentView: View {
    @State var wakeUp = Date.now
    
    var body: some View {
////        1)
//        DatePicker("Please enter a date", selection: $wakeUp)
////        2)
//        DatePicker("", selection: $wakeUp)
////        3)
//        DatePicker("Please enter a date", selection: $wakeUp)
//            .labelsHidden()
////        4)
//        DatePicker("Please enter a date", selection: $wakeUp, displayedComponents: .date)
        DatePicker("Please enter a date", selection: $wakeUp, in: Date.now...)
    }
    
//    func exampleDate() {
//        let tomorrow = Date.now.addingTimeInterval(86400)
//        let range = Date.now...tomorrow
//    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
