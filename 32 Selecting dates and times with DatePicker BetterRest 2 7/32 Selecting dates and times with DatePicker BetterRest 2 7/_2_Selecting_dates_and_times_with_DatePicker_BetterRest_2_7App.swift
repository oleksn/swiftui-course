//
//  _2_Selecting_dates_and_times_with_DatePicker_BetterRest_2_7App.swift
//  32 Selecting dates and times with DatePicker BetterRest 2 7
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _2_Selecting_dates_and_times_with_DatePicker_BetterRest_2_7App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
