//
//  _39_Fixing_Bookworm_Accessibility_6_6App.swift
//  139 Fixing Bookworm Accessibility 6 6
//
//  Created by Oleksandr Nesynov on 02.03.2023.
//

import SwiftUI

@main
struct _39_Fixing_Bookworm_Accessibility_6_6App: App {
    let persistenceContainer = DataController()
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(
                    \.managedObjectContext,
                     persistenceContainer
                        .container
                        .viewContext
                )
        }
    }
}
