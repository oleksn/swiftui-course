//
//  ContentView.swift
//  126 Using Touch ID and Face ID with Bucket List 5 12
//
//  Created by Oleksandr Nesynov on 23.02.2023.
//

import LocalAuthentication
import SwiftUI

struct ContentView: View {
    @State var isUnlock = false
    
    var body: some View {
        VStack {
            if isUnlock {
                Text("Unlocked")
            } else {
                Text("Locked")
            }
        }
        .onAppear(perform: authenticate)
    }
    
    func authenticate() {
        let context = LAContext()
        var error: NSError?
        
        if context.canEvaluatePolicy(
            .deviceOwnerAuthenticationWithBiometrics,
            error: &error
        ) {
            // here
            let reason = "We need to unlock your data"
            context.evaluatePolicy(
                .deviceOwnerAuthenticationWithBiometrics,
                localizedReason: reason) { success, authenticationError in
                    if success {
                        // authenticated successfully
                        isUnlock = true
                    } else {
                        // there was a problem
                    }
                }
        } else {
            // no biometrics
            
        }
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
