//
//  _26_Using_Touch_ID_and_Face_ID_with_Bucket_List_5_12App.swift
//  126 Using Touch ID and Face ID with Bucket List 5 12
//
//  Created by Oleksandr Nesynov on 23.02.2023.
//

import SwiftUI

@main
struct _26_Using_Touch_ID_and_Face_ID_with_Bucket_List_5_12App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
