//
//  AddView.swift
//  58 Building a list we can delete from iExpense 7 11
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

struct AddView: View {
    @StateObject var expenses = Expenses()
    
    @State var name = ""
    @State var type = "Personal"
    @State var amount = 0.0
    
    let types = ["Business, Personal"]
    
    var body: some View {
        NavigationView {
            Form {
                TextField("Name", text: $name)
                
                Picker("Type", selection: $type) {
                    ForEach(types, id: \.self) {
                        Text($0)
                        }
                }
                
                TextField(
                    "Amount",
                    value: $amount,
                    format: .currency(code: "USD")
                )
                .keyboardType(.decimalPad)
                
//                Text(item.amount, format: )
            }
            .navigationTitle("Add new expense")
        }
    }
}

struct AddView_Previews: PreviewProvider {
    static var previews: some View {
        AddView(expenses: Expenses())
    }
}
