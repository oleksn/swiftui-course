//
//  _8_Building_a_list_we_can_delete_from_iExpense_7_11App.swift
//  58 Building a list we can delete from iExpense 7 11
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _8_Building_a_list_we_can_delete_from_iExpense_7_11App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
