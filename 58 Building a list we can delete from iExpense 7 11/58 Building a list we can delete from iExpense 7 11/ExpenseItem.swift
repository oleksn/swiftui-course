//
//  ExpenseItem.swift
//  58 Building a list we can delete from iExpense 7 11
//
//  Created by Oleks on 03.02.2023
//

import Foundation

struct ExpenseItem: Identifiable {
    let id = UUID()
    let name: String
    let type: String
    let amount: Double
}
