//
//  _6_Animating_bindings_Animation_3_8App.swift
//  46 Animating bindings Animation 3 8
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _6_Animating_bindings_Animation_3_8App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
