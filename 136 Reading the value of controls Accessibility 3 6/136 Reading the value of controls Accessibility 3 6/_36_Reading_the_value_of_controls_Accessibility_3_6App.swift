//
//  _36_Reading_the_value_of_controls_Accessibility_3_6App.swift
//  136 Reading the value of controls Accessibility 3 6
//
//  Created by Oleksandr Nesynov on 02.03.2023.
//

import SwiftUI

@main
struct _36_Reading_the_value_of_controls_Accessibility_3_6App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
