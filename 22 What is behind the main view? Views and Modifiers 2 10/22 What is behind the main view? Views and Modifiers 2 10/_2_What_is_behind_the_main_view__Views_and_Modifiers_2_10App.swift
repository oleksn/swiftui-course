//
//  _2_What_is_behind_the_main_view__Views_and_Modifiers_2_10App.swift
//  22 What is behind the main view? Views and Modifiers 2 10
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _2_What_is_behind_the_main_view__Views_and_Modifiers_2_10App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
