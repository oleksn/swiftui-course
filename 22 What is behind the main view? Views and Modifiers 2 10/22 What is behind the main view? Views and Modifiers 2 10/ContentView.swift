//
//  ContentView.swift
//  22 What is behind the main view? Views and Modifiers 2 10
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            Text("Hello, world!")
                .frame(
                    maxWidth: .infinity,
                    maxHeight: .infinity
                )
                .background(.red)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
