//
//  _6_Creating_books_with_Core_Data_Bookworm_4_10App.swift
//  96 Creating books with Core Data Bookworm 4 10
//
//  Created by Oleks on 16.02.2023.
//

import SwiftUI

@main
struct _6_Creating_books_with_Core_Data_Bookworm_4_10App: App {
    @StateObject var dataController = DataController()
    
    var body: some Scene {
        WindowGroup {
            ContentView()
            ContentView()
                .environment(\.managedObjectContext, dataController.container.viewContext)
        }
    }
}
