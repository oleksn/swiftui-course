//
//  _9_Working_with_Identifiable_items_in_iExpense_8_11App.swift
//  59 Working with Identifiable items in iExpense 8 11
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _9_Working_with_Identifiable_items_in_iExpense_8_11App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
