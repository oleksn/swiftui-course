//
//  ContentView.swift
//  9 Adding a segmented control for tip percentages WeSplit 9 11
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

struct ContentView: View {
    @State var check = 0.0
    @State var number = 2
    @State var tip = 20
    
    let tips = [10, 15, 20, 25, 0]
    
    var body: some View {
        NavigationView {
            Form {
                Section {
                    TextField("Amount", value: $check,
                              //                    format: .currency(code: "USD")
                              format: .currency(
                                code: Locale.current.currency?.identifier ?? "USD"
                              )
                    )
                    .keyboardType(.decimalPad)
                    
                    Picker("Number of people", selection: $number) {
                        ForEach(2..<100) {
                            Text("\($0) people")
                        }
                    }
                }
                
                Section {
//                    Text("Header")
                    Picker("Percentage", selection: $tip) {
                        ForEach(tips, id: \.self) {
                            Text($0, format: .percent)
                        }
                    }.pickerStyle(.segmented)
                } header: {
                    Text("Header")
                }
                
                Section {
                    Text(check, format: .currency(
                        code: Locale.current.currency?.identifier ?? "USD"
                    ))
                }
            }
        }.navigationTitle("navigationTitle")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
