//
//  _5_How_to_combine_Core_Data_and_SwiftUI_Bookworm_3_10App.swift
//  95 How to combine Core Data and SwiftUI Bookworm 3 10
//
//  Created by Oleks on 16.02.2023.
//

import SwiftUI

@main
struct _5_How_to_combine_Core_Data_and_SwiftUI_Bookworm_3_10App: App {
    @StateObject var dataController = DataController()
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, dataController.container.viewContext)
        }
    }
}
