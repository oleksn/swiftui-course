//
//  ContentView.swift
//  45 Customizing animations in Animation 2 8
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

struct ContentView: View {
    @State var animationAmount = 1.0
    
    var body: some View {
//        Button("Tap Me") {
//            animationAmount += 1
//        }
//        .padding(50)
//        .background(.red)
//        .foregroundColor(.white)
//        .clipShape(Circle())
////// 5
//        .overlay(
//            Circle()
//                .stroke(.red)
//                .scaleEffect(animationAmount)
//                .opacity(2 - animationAmount)
//
//        )
//        .scaleEffect(animationAmount)
//        .blur(radius: (animationAmount - 1) * 3)
////// 1
////        .animation(.default, value: animationAmount)
////// 2
////        .animation(.interpolatingSpring(
////            stiffness: 50,
////            damping: 1
////        ), value: animationAmount)
////// 3
////        .animation(
////            .easeInOut(duration: 2).delay(1),
////            value: animationAmount
////        )
////// 4
//        .animation(
//            .easeInOut(duration: 1)
//            // .repeatCount(2, autoreverses: true), // jump with 2!
//            // .repeatCount(3, autoreverses: true),
//            .repeatForever(autoreverses: true),
//            value: animationAmount
//        )

////// 6
        Button("Tap Me") {
            
        }
        .padding(50)
        .background(.red)
        .foregroundColor(.white)
        .clipShape(Circle())
        .overlay(
            Circle()
                .stroke(.red)
                .scaleEffect(animationAmount)
                .opacity(2 - animationAmount)
                .animation(
                    .easeInOut(duration: 1)
                    .repeatForever(autoreverses: false),
                    value: animationAmount
                )
        )
        .onAppear {
            animationAmount = 2
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
