//
//  _5_Customizing_animations_in_Animation_2_8App.swift
//  45 Customizing animations in Animation 2 8
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _5_Customizing_animations_in_Animation_2_8App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
