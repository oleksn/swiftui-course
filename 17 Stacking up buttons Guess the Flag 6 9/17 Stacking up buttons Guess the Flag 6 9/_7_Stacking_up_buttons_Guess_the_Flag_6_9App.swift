//
//  _7_Stacking_up_buttons_Guess_the_Flag_6_9App.swift
//  17 Stacking up buttons Guess the Flag 6 9
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _7_Stacking_up_buttons_Guess_the_Flag_6_9App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
