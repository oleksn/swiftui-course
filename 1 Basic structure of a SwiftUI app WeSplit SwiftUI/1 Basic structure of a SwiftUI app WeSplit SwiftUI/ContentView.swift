//
//  ContentView.swift
//  1 Basic structure of a SwiftUI app WeSplit SwiftUI
//
//  Created by Oleks on 27.01.2023.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            Text("Hello, world!")
                .padding()
        }
        .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
