//
//  __Basic_structure_of_a_SwiftUI_app_WeSplit_SwiftUIApp.swift
//  1 Basic structure of a SwiftUI app WeSplit SwiftUI
//
//  Created by Oleks on 27.01.2023.
//

import SwiftUI

@main
struct __Basic_structure_of_a_SwiftUI_app_WeSplit_SwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
