//
//  _46_Adding_custom_row_swipe_actions_to_a_List_Hot_Prospects_7_18App.swift
//  146 Adding custom row swipe actions to a List Hot Prospects 7 18
//
//  Created by Oleksandr Nesynov on 03.03.2023.
//

import SwiftUI

@main
struct _46_Adding_custom_row_swipe_actions_to_a_List_Hot_Prospects_7_18App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
