//
//  ContentView.swift
//  146 Adding custom row swipe actions to a List Hot Prospects 7 18
//
//  Created by Oleksandr Nesynov on 03.03.2023.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        List {
            Text("Taylor Swift")
                .swipeActions {
                    Button(role: .destructive) {
                        print("Hi")
                    } label: {
//                        Label(
//                            "Send message",
//                            systemImage: "message"
//                        )
                        Label(
                            "Delete",
                            systemImage: "minus.circle"
                        )
                    }
                }
                .swipeActions(edge: .leading) {
                    Button {
                        print("Pinning: ")
                    } label: {
                        Label("Pin", systemImage: "pin")
                    }
                    .tint(.orange)
                }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
