//
//  _6_Environment_modifiers_Views_and_Modifiers_6_10App.swift
//  26 Environment modifiers Views and Modifiers 6 10
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _6_Environment_modifiers_Views_and_Modifiers_6_10App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
