//
//  ContentView.swift
//  26 Environment modifiers Views and Modifiers 6 10
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            Text("Hello, world! 1")
                .font(.largeTitle)
//                .blur(radius: 0)
            Text("Hello, world! 2")
            Text("Hello, world! 3")
            Text("Hello, world! 4")
        }
        .font(.title)
//        .blur(radius: 5)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
