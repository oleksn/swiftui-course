//
//  _7_Creating_explicit_animations_Animation_4_8App.swift
//  47 Creating explicit animations Animation 4 8
//
//  Created by Oleks on 03.02.2023
//

import SwiftUI

@main
struct _7_Creating_explicit_animations_Animation_4_8App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
