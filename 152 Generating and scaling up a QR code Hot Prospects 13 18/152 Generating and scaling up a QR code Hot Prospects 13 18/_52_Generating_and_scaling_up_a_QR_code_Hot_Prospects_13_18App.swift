//
//  _52_Generating_and_scaling_up_a_QR_code_Hot_Prospects_13_18App.swift
//  152 Generating and scaling up a QR code Hot Prospects 13 18
//
//  Created by Oleksandr Nesynov on 03.03.2023.
//

import SwiftUI

@main
struct _52_Generating_and_scaling_up_a_QR_code_Hot_Prospects_13_18App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
