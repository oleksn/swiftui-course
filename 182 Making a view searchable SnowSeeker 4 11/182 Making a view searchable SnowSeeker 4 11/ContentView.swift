//
//  ContentView.swift
//  182 Making a view searchable SnowSeeker 4 11
//
//  Created by Oleksandr Nesynov on 10.03.2023.
//

import SwiftUI

struct ContentView: View {
    @State var searchText = ""
    let allNames = ["Subh", "Vina", "Melvin", "Stefanie"]
    
    var body: some View {
        NavigationView {
////        // 1)
//            Text("Searching for \(searchText)")
//                .searchable(
//                    text: $searchText,
//                    prompt: "Search"
//                )
//                .navigationTitle("Searching")
            
            List(filteredNames, id: \.self) { name in
                Text(name)
            }
            .searchable(
                text: $searchText,
                prompt: "Search"
            )
            .navigationTitle("Searching")
        }
    }
    
    var filteredNames: [String] {
        if searchText.isEmpty {
            return allNames
        } else {
//            return allNames.filter { $0.contains(searchText) }
            return allNames.filter { $0.localizedCaseInsensitiveContains(searchText) }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
