//
//  _82_Making_a_view_searchable_SnowSeeker_4_11App.swift
//  182 Making a view searchable SnowSeeker 4 11
//
//  Created by Oleksandr Nesynov on 10.03.2023.
//

import SwiftUI

@main
struct _82_Making_a_view_searchable_SnowSeeker_4_11App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
