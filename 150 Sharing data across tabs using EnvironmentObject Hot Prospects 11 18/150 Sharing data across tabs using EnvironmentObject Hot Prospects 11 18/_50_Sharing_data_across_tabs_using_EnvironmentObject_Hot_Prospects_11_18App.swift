//
//  _50_Sharing_data_across_tabs_using_EnvironmentObject_Hot_Prospects_11_18App.swift
//  150 Sharing data across tabs using EnvironmentObject Hot Prospects 11 18
//
//  Created by Oleksandr Nesynov on 03.03.2023.
//

import SwiftUI

@main
struct _50_Sharing_data_across_tabs_using_EnvironmentObject_Hot_Prospects_11_18App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
