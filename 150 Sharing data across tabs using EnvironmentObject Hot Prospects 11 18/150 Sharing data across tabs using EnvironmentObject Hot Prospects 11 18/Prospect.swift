//
//  Prospect.swift
//  150 Sharing data across tabs using EnvironmentObject Hot Prospects 11 18
//
//  Created by Oleksandr Nesynov on 03.03.2023.
//

import SwiftUI

class Prospect: Identifiable, Codable {
    var id = UUID()
    var name = "Anonymous"
    var emailAddress = ""
    var isContacted = false
}

@MainActor class Prospects: ObservableObject {
    @Published var people: [Prospect]
    
    init() {
        people = []
    }
}
